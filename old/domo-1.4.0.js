/***
 * Library - Domo
 *
 * Useful methods for DOM elements
 ***/
(function(global) {
	'use strict';	
	
	/**
	 * Returns the content of a meta tag by its name
	 * @param: {string} name
	 * @return: {string | null}
	 */
	function getMeta(name) {
		if (typeof name !== 'string') {
			throw new TypeError('getMeta() - expects string argument, got: ' + typeof name);
		}
		
		var metas = document.getElementsByTagName('meta');
		
		// Look through all existing meta tags
		var i,len; // loop vars
		if (name.indexOf('og:') === 0) {
			for (i = 0, len = metas.length; i<len; i++) {
				if (name === metas[i].getAttribute('property')) {
					return metas[i].content;
				}
			}
		} // Open Graph tag
		else {
			for (i = 0, len = metas.length; i<len; i++) {
				if (name === metas[i].name) {
					return metas[i].content;
				}
			}
		} // Normal meta
		return null;
	}
	
	/**
	 * Sets a meta in <head>. If meta does not exist, set it.
	 * - Note: if name is Open Graph property, i.e. starts with "og:", the property attribute is set instead.
	 * @param: {string} name
	 *         {string} content
	 * @return: {HTMLMetaElement}
	 */
	function setMeta(name, content) {
		if (typeof name !== 'string') {
			throw new TypeError('setMeta() - expects string argument for name, got: ' + typeof name);
		}
		if (typeof content !== 'string') {
			throw new TypeError('setMeta() - expects string argument for content, got: ' + typeof content);
		}
		
		var metas = document.getElementsByTagName('meta');
		// Look through all existing meta tags
		var i,len; // loop vars
		for (i = 0, len = metas.length; i<len; i++) {
			if (name === metas[i].name) {
				metas[i].content = content;
				return metas[i];
			} // Normal meta
			if (name === metas[i].getAttribute('property')) {
				metas[i].content = content;
				return metas[i];
			} // Open Graph tag
		}
		
		// Add a new meta
		var tag = document.createElement('meta');
		if (name.indexOf('og:') === 0) {
			tag.setAttribute('property', name);
		} // Open Graph tag
		else {
			tag.name = name;
		}
		tag.content = content;
		document.getElementsByTagName('head')[0].appendChild(tag);
		return tag;
	} // setMeta()
	
	/**
	 * @param: {string} file path
	 *         {object} data (optional) - key-value pairs of HTML5 data attributes
	 * @return: {HTMLLinkElement}
	 */
	function addCSS(file_path, data) {
		if (typeof file_path !== 'string') {
			throw new TypeError('addCSS() - expects string argument for file path, got: ' + typeof file_path);
		}
		if (typeof data !== 'object') {
			data = {};
		}
		
		// Create <link> with href and data-* attributes
		var link = document.createElement('link');
		link.setAttribute('rel', 'stylesheet');
		link.setAttribute('href', file_path);
		for (var prop in data) {
			if (!data.hasOwnProperty(prop)) {
				continue;
			}
			
			link.setAttribute('data-'+prop, data[prop]);
		}
		document.getElementsByTagName('head')[0].appendChild(link);
		
		return link;
	} //addCSS()
	
	/**
	 * @param: {string} file path
	 * @return: {number} of link tags removed
	 */
	function removeCSS(file_path) {
		var links = [].slice.call(document.getElementsByTagName('link'));
		var removed_count = 0;
		for (var i=0,len=links.length; i<len; i++) {
			var link = links[i];
			if (link.getAttribute('href') === file_path) {
				link.setAttribute('disabled', 'disabled');
				link.parentNode.removeChild(link);
				removed_count++;
			}
		}
		
		return removed_count;
	} //removeCSS()
	
	/**
	 * @param: {object} data attributes to check against
	 * @return: {number} of link tags removed
	 */
	function removeCSSByData(data) {
		if (typeof data !== 'object') {
			return 0;
		}
		
		var links = [].slice.call(document.getElementsByTagName('link'));
		var removed_count = 0;
		for (var i=0; i<links.length; i++) {
			var link = links[i];
			if (hasAllData(data, link)) {
				link.setAttribute('disabled', 'disabled');
				link.parentNode.removeChild(link);
				removed_count++;
			}
		}


		
		//console.log('removing ' + removed_count + ' CSS');
		return removed_count;
	} //removeCSSByData()
	
	/**
	 * @param: {string} file path
	 *         {object} data (optional) - key-value pairs of HTML5 data attributes
	 * @return: {HTMLScriptElement}
	 */
	function addJS(file_path, data) {
		if (typeof file_path !== 'string') {
			throw new TypeError('addJS() - expects string argument for file path, got: ' + typeof file_path);
		}
		if (typeof data !== 'object') {
			data = {};
		}
		
		// Create <script> with src and data-* attributes
		var script = document.createElement('script');
		script.type = 'text/javascript';
		script.src = file_path;
		for (var prop in data) {
			if (!data.hasOwnProperty(prop)) {
				continue;
			}
			
			script.setAttribute('data-'+prop, data[prop]);
		}
		
		document.body.appendChild(script);
		
		return script;
	} //addJS()
	
	/**
	 * @param: {string} file path
	 * @return: {number} of script tags removed
	 */
	function removeJS(file_path) {
		var scripts = [].slice.call(document.getElementsByTagName('script'));
		var removed_count = 0;
		for (var i=0,len=scripts.length; i<len; i++) {
			var script = scripts[i];
			if (script.getAttribute('src') === file_path) {
				script.parentNode.removeChild(script);
				removed_count++;
			}
		}
		
		return removed_count;
	} //removeJS
	
	/**
	 * @param: {object} data attributes to check against
	 * @return: {number} of script tags removed
	 */
	function removeJSByData(data) {
		if (typeof data !== 'object') {
			return 0;
		}
		
		var scripts = [].slice.call(document.getElementsByTagName('script'));
		var removed_count = 0;
		for (var i=0,len=scripts.length; i<len; i++) {
			var script = scripts[i];
			if (hasAllData(data, script)) {
				script.parentNode.removeChild(script);
				removed_count++;
			}
		}
		
		//console.log('removing ' + removed_count + ' JS');
		return removed_count;
	} //removeJSByData()
	
	/**
	 * @private
	 * Checks if an elem has all data key-value pairs described
	 */
	function hasAllData(data, elem) {
		for (var key in data) {
			if (!data.hasOwnProperty(key)) {
				continue;
			}
			
			if (elem.getAttribute('data-'+key) !== data[key]) {
				return false;
			}
		}
		
		return true;
	} //hasAllData()

	/**
	 * Gets the text currently selected (highlighted)
	 * @return: {string}
	 */
	function getSelectedText() {
		if (window.getSelection) { // Other browsers
			return window.getSelection().toString();
		}
		else if (document.selection) {
			return document.selection.createRange().htmlText;
		}
	} //getSelectedText()
	
	/**
	 * Selects all text in an element (akin to highlighting with your mouse)
	 * @param: {HTMLElement}
	 */
	function selectText(elem) {
		if (typeof elem !== 'object' || !(elem instanceof HTMLElement)) {
			throw new TypeError('selectText() - param is not an HTMLElement.');
		}
		
		var range, selection;
		
		if (document.body.createTextRange) { // IE
			range = document.body.createTextRange();
			range.moveToElementText(elem);
			range.select();
		}
		else if (window.getSelection) { // Other browsers
			selection = window.getSelection();        
			range = document.createRange();
			range.selectNodeContents(elem);
			selection.removeAllRanges();
			selection.addRange(range);
		}
	} //selectText()
	
	/**
	 * Locks scroll position of an element, and removes scrollbars
	 * - Note: if already locked, operation will fail
	 * @param: {HTMLElement}
	 * @return: {bool} success
	 */
	function lockScroll(elem) {
		var state = elem.getAttribute('data-locked-scroll');
		if (state === 'locked') {
			//console.log('Already locked');
			return false;
		}
		
		// Remember scroll position
		elem.setAttribute('data-locked-scroll-left', elem.scrollLeft);
		elem.setAttribute('data-locked-scroll-top', elem.scrollTop);
		elem.setAttribute('data-locked-scroll', 'locked');
		
		// Remember overflow setting
		var cs = window.getComputedStyle(elem, null);
		elem.setAttribute('data-previous-overflow-x', cs.overflowX);
		elem.setAttribute('data-previous-overflow-y', cs.overflowY);
		
		// Set elem overflow
		elem.style.overflowX = 'hidden';
		elem.style.overflowY = 'hidden';
		// DEBUG
		//console.log('Set overflowX: ' + elem.style.overflowX);
		//console.log('Set overflowY: ' + elem.style.overflowY);
		
		return true;
	} //lockScroll()
	
	/**
	 * Unlocks an element that was locked by lockScroll()
	 * @param: {HTMLElement}
	 * @return: {boolean} success
	 */
	function unlockScroll(elem) {
		var state = elem.getAttribute('data-locked-scroll');
		if (state !== 'locked') {
			//console.log('Not locked');
			return false;
		}
		
		// Recall overflow settings
		elem.style.overflowX = elem.getAttribute('data-previous-overflow-x');
		elem.style.overflowY = elem.getAttribute('data-previous-overflow-y');
		elem.removeAttribute('data-previous-overflow-x');
		elem.removeAttribute('data-previous-overflow-y');
		// DEBUG
		//console.log('Reset overflowX: ' + elem.style.overflowX);
		//console.log('Reset overflowX: ' + elem.style.overflowY);
		
		// Recall scroll position
		elem.scrollLeft = elem.getAttribute('data-locked-scroll-left');
		elem.scrollTop = elem.getAttribute('data-locked-scroll-top');
		elem.removeAttribute('data-locked-scroll-top');
		elem.removeAttribute('data-locked-scroll-left');
		elem.removeAttribute('data-locked-scroll');
		
		return true;
	} //unlockScroll
	
	/**
	 * Gets position of element w.r.t. screen, i.e. does not include scrollTop & scrollLeft amounts.
	 * @param: {HTMLElement} elem
	 * @return: {object} point coordinates, e.g. {x:1, y:2}
	 */
	function getPositionOnScreen(elem) {
		if (typeof elem !== 'object' || !(elem instanceof HTMLElement)) {
			throw new TypeError('getPositionOnScreen() - param is not an HTMLElement.');
		}
		
		var elem_rect = elem.getBoundingClientRect();
		var offsetTop = elem_rect.top - document.documentElement.clientTop;
		var offsetLeft = elem_rect.left - document.documentElement.clientLeft;

		return {x:offsetLeft, y:offsetTop};
	}
	
	/**
	 * Requests full screen using FullScreen API. Useful for mobile.
	 * - Note: must be called only as a result of a user-generated event
	 * - Note: document must allow fullscreen, e.g. not enabled for iframes without "allowfullscreen" attribute
	 * - Note: requires browser support of fullscreen API
	 * @param: {HTMLElement} elem - default: documentElement
	 * @return: {bool} success
	 */
	function requestFullScreen(elem) {
		if (!(elem instanceof HTMLElement)) {
			elem = document.documentElement;
		}
		
		if (elem.requestFullscreen && document.fullscreenEnabled) {
			elem.requestFullscreen();
			return true;
		}
		else {
			console.error('requestFullScreen() - Full Screen API not supported or is not allowed to be enabled at this time.');
			return false;
		}
	} //requestFullScreen()
	
	/**
	 * Converts a string containing HTML into DOM Elements
	 * @param: {string}
	 * @return: {Array} of HTMLElement
	 */
	function str2dom(s) {
		var div = document.createElement('div');
		div.innerHTML = s;
		return [].slice.call(div.children); // because we like stable, non-live lists
	}

	/**
	 * Converts Table or Form element into a CSV.
	 * - Note for tables:
	 *       if parseHeader is false, rows in <thead> will be skipped.
	 *       <thead> is expected to occur at most once, and only at the beginning.
	 * - Note for forms:
	 *       if parseHeader is true, returns a line containing all names/IDs, and a line containing all values.
	 *       if praseHeader is false, returns a single-line CSV of values only.
	 *       either name or id attribute need to be present, else element is ignored.
	 *
	 * @param: {HTMLTableElement | HTMLFormElement} elem
	 *         {object} options (optional)
	 *                  - {string} delimiter (defaults to comma. Must be 1 char)
	 *                  - {string} lineEnding (defaults to \n. Max of 2 chars)
	 *                  - {string} quote {defaults to ". Must be 1 char)
	 *                  - {boolean} parseHeader (defaults to true)
	 *                  - {boolean} trim (defaults to true)
	 *                  - {boolean} unescapeHTML (defaults to true for table elements, false otherwise)
	 * @return: {string}
	 */
	function elem2csv(elem, options) {
		if (typeof elem !== 'object' || !(elem instanceof HTMLElement)) {
			throw new TypeError('elem2csv() - param is not an HTMLElement.');
		}
		
		// Process options
		if (typeof options !== 'object' || options === null) {
			options = {
				'delimeter': ',',
				'lineEnding': '\n',
				'quote': '"',
				'trim': true,
				'parseHeader': true
			};
		}
		if (typeof options['delimeter'] !== 'string' || options['delimeter'].length !== 1) {
			options['delimeter'] = ',';
		}
		if (typeof options['lineEnding'] !== 'string' || options['lineEnding'].length > 2 || options['lineEnding'].length < 1) {
			options['lineEnding'] = '\n';
		}
		if (typeof options['quote'] !== 'string' || options['quote'].length !== 1) {
			options['quote'] = '"';
		}
		if (typeof options['trim'] !== 'boolean') {
			options['trim'] = true;
		}
		if (typeof options['parseHeader'] !== 'boolean') {
			options['parseHeader'] = true;
		}
		
		// Process according to element type
		var i, j, row_count, cell_count, row, rowData, cell, name, value; // tmp variables for loops

		if (elem instanceof HTMLTableElement) {
			if (typeof options.unescapeHTML !== 'boolean') {
				options.unescapeHTML = true;
			}
			var csvData = [];
			
			row_count=elem.rows.length;
			i = options['parseHeader'] ? 0 : elem.tHead.rows.length;
			for (; i<row_count; i++) {
				row = elem.rows[i];
				
				rowData = [];
				for (j=0, cell_count=row.cells.length; j<cell_count; j++) {
					cell = row.cells[j];
					value = cell.innerHTML;
					if (options.unescapeHTML) {
						value = unescapeHTML(value);
					}
					
					rowData.push( processCellValue(value, options) );
				}
				csvData.push(rowData.join(options['delimeter']));
			} // for each row
			
			return csvData.join(options['lineEnding']);
		} //HTMLTableElement
		else if (elem instanceof HTMLFormElement) {
			if (typeof options.unescapeHTML !== 'boolean') {
				options.unescapeHTML = false;
			}
			
			var nameData = [];
			var valueData = [];
			for (i=0, row_count = elem.elements.length; i<row_count; i++) {
				try {
					name = processCellValue(elem.elements[i].name, options) || processCellValue(elem.elements[i].id, options);
					value = processCellValue(elem.elements[i].value, options);
					if (options.unescapeHTML) {
						name = unescapeHTML(name);
						value = unescapeHTML(value);
					}
				}
				catch (e) {
					continue;
				}
				
				nameData.push(name);
				valueData.push(value);
			}
			
			if (options['parseHeader']) {
				return nameData.join(options['delimeter']) + options['lineEnding'] + valueData.join(options['delimeter']);
			}
			else {
				return valueData.join(options['delimeter']);
			}
		} //HTMLFormElement
		else {
			throw new TypeError('elem2csv() - unsupported elem type.');
		}
	} //elem2csv()
	
	/**
	 * @private
	 * Turns HTML escaped text into human-readable text
	 */
	function unescapeHTML(value) {
		// convert HTML code into human-readable text
		return value.replace(/&amp;/g,'&')
			.replace(/&lt;/g,'<')
			.replace(/&gt;/g,'>')
			.replace(/&quot;/g,'"')
			.replace(/&#x27;/g, '\'').replace(/&#039;/g, '\'')
			.replace(/&#x2F;/g, '\/').replace(/&#047;/g, '\/')
			.replace(/\u00a0/g, ' ');
	}
	/**
	 * @private
	 * Turns text into safe CSV cell value
	 */
	function processCellValue(value, options) {
		// values containing line endings, quotes and commas must be enclosed 
		if (value.indexOf(options['delimeter']) !== -1 || value.indexOf(options['quote']) !== -1 || value.indexOf(options['lineEnding']) !== -1) {
			var regex = new RegExp(options['quote'], 'g');
			value =
				options['quote'] +
				value.replace(regex, options['quote']+options['quote']) + // escape quotes
				options['quote'];
		} 
	
		return value;
	} //processCellValue()
	
	global.domo = {
		/* DOM global meta */
		'getMeta': getMeta,
		'setMeta': setMeta,
		'addCSS': addCSS,
		'addJS': addJS,
		'removeCSS': removeCSS,
		'removeCSSByData': removeCSSByData,
		'removeJS': removeJS,
		'removeJSByData': removeJSByData,
		'getSelectedText': getSelectedText,

		/* DOM element functions */
		'selectText': selectText,
		'lockScroll': lockScroll,
		'unlockScroll': unlockScroll,
		'requestFullScreen': requestFullScreen,
		'getPositionOnScreen': getPositionOnScreen,
		'str2dom': str2dom,
		'elem2csv': elem2csv
	};
})(this);
