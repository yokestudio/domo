/***
 * Library - Domo
 *
 * Useful methods for DOM elements
 ***/
(function(global, window, document) {
	'use strict';	
	
	// Context
	var checkbox_radio_counter = 0;  // used to count how many checkboxes/radios we have made so far

	/**
	 * @private
	 * Checks if an elem has all data key-value pairs described
	 * @param: {object} data attributes to check
	 */
	function hasAllData(data, elem) {
		for (var key in data) {
			if (!data.hasOwnProperty(key)) {
				continue;
			}
			
			if (elem.getAttribute('data-'+key) !== data[key]) {
				return false;
			}
		}
		
		return true;
	} //hasAllData()

	/**
	 * @private
	 * Inverse function of escapeHTML()
	 * @param {string}
	 * @return {string}
	 */
	function unescapeHTML(str) {
		if (typeof str !== 'string') {
			throw new TypeError('unescapeHTML() - param is not str: ' + str);
		}
		return str
			.replace(/&amp;/g,'&')
			.replace(/&lt;/g,'<')
			.replace(/&gt;/g,'>')
			.replace(/&quot;/g,'"')
			.replace(/&#x27;/g, '\'').replace(/&#039;/g, '\'')
			.replace(/&#x2F;/g, '\/').replace(/&#047;/g, '\/')
			.replace(/\u00a0/g, ' ');
	} //unescapeHTML()

	/**
	 * @private
	 * Turns text into safe CSV cell value
	 */
	function processCellValue(value, options) {
		// values containing line endings, quotes and commas must be enclosed 
		if (value.indexOf(options['delimiter']) !== -1 || value.indexOf(options['quote']) !== -1 || value.indexOf(options['lineEnding']) !== -1) {
			var regex = new RegExp(options['quote'], 'g');
			value =
				options['quote'] +
				value.replace(regex, options['quote']+options['quote']) + // escape quotes
				options['quote'];
		} 
	
		return value;
	} //processCellValue()


	/**
	 * Returns the content of a meta tag by its name
	 * @param: {string} name
	 * @return: {string | null}
	 */
	function getMeta(name) {
		if (typeof name !== 'string') {
			throw new TypeError('getMeta() - expects string argument, got: ' + typeof name);
		}
		
		var metas = document.getElementsByTagName('meta');
		
		// Look through all existing meta tags
		var i,len; // loop vars
		if (name.indexOf('og:') === 0) {
			for (i = 0, len = metas.length; i<len; i++) {
				if (name === metas[i].getAttribute('property')) {
					return metas[i].content;
				}
			}
		} // Open Graph tag
		else {
			for (i = 0, len = metas.length; i<len; i++) {
				if (name === metas[i].name) {
					return metas[i].content;
				}
			}
		} // Normal meta
		return null;
	}
	
	/**
	 * Sets a meta in <head>. If meta does not exist, set it.
	 * - Note: if name is Open Graph property, i.e. starts with "og:", the property attribute is set instead.
	 * @param: {string} name
	 *         {string} content
	 * @return: {HTMLMetaElement}
	 */
	function setMeta(name, content) {
		if (typeof name !== 'string') {
			throw new TypeError('setMeta() - expects string argument for name, got: ' + typeof name);
		}
		if (typeof content !== 'string') {
			throw new TypeError('setMeta() - expects string argument for content, got: ' + typeof content);
		}
		
		var metas = document.getElementsByTagName('meta');
		// Look through all existing meta tags
		var i,len; // loop vars
		for (i = 0, len = metas.length; i<len; i++) {
			if (name === metas[i].name) {
				metas[i].content = content;
				return metas[i];
			} // Normal meta
			if (name === metas[i].getAttribute('property')) {
				metas[i].content = content;
				return metas[i];
			} // Open Graph tag
		}
		
		// Add a new meta
		var tag = document.createElement('meta');
		if (name.indexOf('og:') === 0) {
			tag.setAttribute('property', name);
		} // Open Graph tag
		else {
			tag.name = name;
		}
		tag.content = content;
		document.getElementsByTagName('head')[0].appendChild(tag);
		return tag;
	} // setMeta()
	
	/**
	 * @param: {string} file path
	 *         {object} data (optional) - key-value pairs of HTML5 data attributes
	 * @return: {HTMLLinkElement}
	 */
	function addCSS(file_path, data) {
		if (typeof file_path !== 'string') {
			throw new TypeError('addCSS() - expects string argument for file path, got: ' + typeof file_path);
		}
		if (typeof data !== 'object') {
			data = {};
		}
		
		// Create <link> with href and data-* attributes
		var link = document.createElement('link');
		link.setAttribute('rel', 'stylesheet');
		link.setAttribute('href', file_path);
		for (var prop in data) {
			if (!data.hasOwnProperty(prop)) {
				continue;
			}
			
			link.setAttribute('data-'+prop, data[prop]);
		}
		document.getElementsByTagName('head')[0].appendChild(link);
		
		return link;
	} //addCSS()
	
	/**
	 * @param: {string} file path
	 * @return: {number} of link tags removed
	 */
	function removeCSS(file_path) {
		var links = [].slice.call(document.getElementsByTagName('link'));
		var removed_count = 0;
		for (var i=0,len=links.length; i<len; i++) {
			var link = links[i];
			if (link.getAttribute('href') === file_path) {
				link.setAttribute('disabled', 'disabled');
				link.parentNode.removeChild(link);
				removed_count++;
			}
		}
		
		return removed_count;
	} //removeCSS()
	
	/**
	 * @param: {object} data attributes to check against
	 * @return: {number} of link tags removed
	 */
	function removeCSSByData(data) {
		if (typeof data !== 'object') {
			return 0;
		}
		
		var links = [].slice.call(document.getElementsByTagName('link'));
		var removed_count = 0;
		for (var i=0; i<links.length; i++) {
			var link = links[i];
			if (hasAllData(data, link)) {
				link.setAttribute('disabled', 'disabled');
				link.parentNode.removeChild(link);
				removed_count++;
			}
		}


		
		//console.log('removing ' + removed_count + ' CSS');
		return removed_count;
	} //removeCSSByData()
	
	/**
	 * @param: {string} file path
	 *         {object} data (optional) - key-value pairs of HTML5 data attributes
	 * @return: {HTMLScriptElement}
	 */
	function addJS(file_path, data) {
		if (typeof file_path !== 'string') {
			throw new TypeError('addJS() - expects string argument for file path, got: ' + typeof file_path);
		}
		if (typeof data !== 'object') {
			data = {};
		}
		
		// Create <script> with src and data-* attributes
		var script = document.createElement('script');
		script.type = 'text/javascript';
		script.src = file_path;
		for (var prop in data) {
			if (!data.hasOwnProperty(prop)) {
				continue;
			}
			
			script.setAttribute('data-'+prop, data[prop]);
		}
		
		document.body.appendChild(script);
		
		return script;
	} //addJS()
	
	/**
	 * @param: {string} file path
	 * @return: {number} of script tags removed
	 */
	function removeJS(file_path) {
		var scripts = [].slice.call(document.getElementsByTagName('script'));
		var removed_count = 0;
		for (var i=0,len=scripts.length; i<len; i++) {
			var script = scripts[i];
			if (script.getAttribute('src') === file_path) {
				script.parentNode.removeChild(script);
				removed_count++;
			}
		}
		
		return removed_count;
	} //removeJS
	
	/**
	 * @param: {object} data attributes to check against
	 * @return: {number} of script tags removed
	 */
	function removeJSByData(data) {
		if (typeof data !== 'object') {
			return 0;
		}
		
		var scripts = [].slice.call(document.getElementsByTagName('script'));
		var removed_count = 0;
		for (var i=0,len=scripts.length; i<len; i++) {
			var script = scripts[i];
			if (hasAllData(data, script)) {
				script.parentNode.removeChild(script);
				removed_count++;
			}
		}
		
		//console.log('removing ' + removed_count + ' JS');
		return removed_count;
	} //removeJSByData()

	/**
	 * Converts a string containing HTML into DOM Elements
	 * @param: {string}
	 * @return: {Array} of HTMLElement
	 */
	function str2dom(s) {
		var div = document.createElement('div');
		div.innerHTML = s;
		return [].slice.call(div.children); // because we like stable, non-live lists
	} //str2dom()

	/**
	 * Gets the text currently selected (highlighted) in the document
	 * @return: {string}
	 */
	function getSelectedText() {
		if (window.getSelection) { // Other browsers
			return window.getSelection().toString();
		}
		else if (document.selection) {
			return document.selection.createRange().htmlText;
		}
	} //getSelectedText()
	
	/**
	 * Selects all text in an element (akin to highlighting with your mouse)
	 * @param: {HTMLElement}
	 */
	function selectText(elem) {
		if (typeof elem !== 'object' || !(elem instanceof HTMLElement)) {
			throw new TypeError('selectText() - param is not an HTMLElement. (' + typeof elem + ')');
		}
		
		var range, selection;
		
		if (document.body.createTextRange) { // IE
			range = document.body.createTextRange();
			range.moveToElementText(elem);
			range.select();
		}
		else if (window.getSelection) { // Other browsers
			selection = window.getSelection();        
			range = document.createRange();
			range.selectNodeContents(elem);
			selection.removeAllRanges();
			selection.addRange(range);
		}
	} //selectText()

	/**
	 * Converts Table or Form element into a CSV.
	 * - Note for tables:
	 *       if parseHeader is false, rows in <thead> will be skipped.
	 *       <thead> is expected to occur at most once, and only at the beginning.
	 * - Note for forms:
	 *       if parseHeader is true, returns a line containing all names/IDs, and a line containing all values.
	 *       if praseHeader is false, returns a single-line CSV of values only.
	 *       either name or id attribute need to be present, else element is ignored.
	 *
	 * @param: {HTMLTableElement | HTMLFormElement} elem
	 *         {object} options (optional)
	 *                  - {string} delimiter (defaults to comma. Must be 1 char)
	 *                  - {string} lineEnding (defaults to \n. Max of 2 chars)
	 *                  - {string} quote {defaults to ". Must be 1 char)
	 *                  - {boolean} parseHeader (defaults to true)
	 *                  - {boolean} trim (defaults to true)
	 *                  - {boolean} unescapeHTML (defaults to true for table elements, false otherwise)
	 * @return: {string}
	 */
	function elem2csv(elem, options) {
		if (typeof elem !== 'object' || !(elem instanceof HTMLElement)) {
			throw new TypeError('elem2csv() - param is not an HTMLElement. (' + typeof elem + ')');
		}
		
		// Process options
		if (typeof options !== 'object' || options === null) {
			options = {
				'delimiter': ',',
				'lineEnding': '\n',
				'quote': '"',
				'trim': true,
				'parseHeader': true
			};
		}
		if (typeof options['delimiter'] !== 'string' || options['delimiter'].length !== 1) {
			options['delimiter'] = ',';
		}
		if (typeof options['lineEnding'] !== 'string' || options['lineEnding'].length > 2 || options['lineEnding'].length < 1) {
			options['lineEnding'] = '\n';
		}
		if (typeof options['quote'] !== 'string' || options['quote'].length !== 1) {
			options['quote'] = '"';
		}
		if (typeof options['trim'] !== 'boolean') {
			options['trim'] = true;
		}
		if (typeof options['parseHeader'] !== 'boolean') {
			options['parseHeader'] = true;
		}
		
		// Process according to element type
		var i, j, row_count, cell_count, row, rowData, cell, name, value; // tmp variables for loops

		if (elem instanceof HTMLTableElement) {
			if (typeof options.unescapeHTML !== 'boolean') {
				options.unescapeHTML = true;
			}
			var csvData = [];
			
			row_count=elem.rows.length;
			i = options['parseHeader'] ? 0 : elem.tHead.rows.length;
			for (; i<row_count; i++) {
				row = elem.rows[i];
				
				rowData = [];
				for (j=0, cell_count=row.cells.length; j<cell_count; j++) {
					cell = row.cells[j];
					value = cell.innerHTML;
					if (options.unescapeHTML) {
						value = unescapeHTML(value);
					}
					
					rowData.push( processCellValue(value, options) );
				}
				csvData.push(rowData.join(options['delimiter']));
			} // for each row
			
			return csvData.join(options['lineEnding']);
		} //HTMLTableElement
		else if (elem instanceof HTMLFormElement) {
			if (typeof options.unescapeHTML !== 'boolean') {
				options.unescapeHTML = false;
			}
			
			var nameData = [];
			var valueData = [];
			for (i=0, row_count = elem.elements.length; i<row_count; i++) {
				try {
					name = processCellValue(elem.elements[i].name, options) || processCellValue(elem.elements[i].id, options);
					value = processCellValue(elem.elements[i].value, options);
					if (options.unescapeHTML) {
						name = unescapeHTML(name);
						value = unescapeHTML(value);
					}
				}
				catch (e) {
					continue;
				}
				
				nameData.push(name);
				valueData.push(value);
			}
			
			if (options['parseHeader']) {
				return nameData.join(options['delimiter']) + options['lineEnding'] + valueData.join(options['delimiter']);
			}
			else {
				return valueData.join(options['delimiter']);
			}
		} //HTMLFormElement
		else {
			throw new TypeError('elem2csv() - unsupported elem type.');
		}
	} //elem2csv()

	/**
	 * @param: {Node} node - to be wrapped
	 *         {string} tag name of wrapper to be created (DEFAULT: div)
	 * @return: {HTMLElement} wrapper
	 */
	function wrapNode(node, tag_name) {
		if (typeof tag_name !== 'string') {
			tag_name = 'div'
		}
		var wrapper = document.createElement(tag_name);
		node.parentNode.insertBefore(wrapper, node.nextSibling);
	    wrapper.appendChild(node);

	    return wrapper;
	}

	/**
	 * @param: {Node}
	 */
	function removeNode(node) {
		node.parentNode.removeChild(node);
	}

	/**
	 * Removes all <br> children
	 * @param: {HTMLElement}
	 */
	function removeBrChildren(dom_root) {
		// Check params
		if (! (dom_root instanceof HTMLElement)) {
			throw new TypeError('removeBrChildren() - param is not an HTMLElement.  (' + typeof dom_root + ')');
		}
		
		var dom_elems = dom_root.children;

		for (var i=0, len=dom_elems.length; i<len; i++) {
			var dom_elem = dom_elems[i];

			if (dom_elem instanceof HTMLBRElement) {
				removeNode(dom_elem);
				i--;
		        len--;
			}
		}
	} //removeBrChildren()

	/**
	 * Removes descendants elements with no content
	 * - <br> elements are not considered as content
	 * - does not affect self-closing elements.
	  * @param: {HTMLElement} 
	 */
	function removeEmptyElementDescendants(dom_root) {
		// Check params
		if (! (dom_root instanceof HTMLElement)) {
			throw new TypeError('removeEmptyElementDescendants() - param is not an HTMLElement.  (' + typeof dom_root + ')');
		}

		// Go through children
		var dom_elems = dom_root.children;
		for (var i=dom_elems.length-1; i>=0; i--) {
			var dom_elem = dom_elems[i];

			removeEmptyElementDescendants(dom_elem);

			// Ignore elements with text content
			if (dom_elem.textContent.trim() !== '') {
				continue;
			}

			// Ignore self-closeing elements
			if (isSelfClosingElement(dom_elem)) {
				continue;
			}

			// If we reach here, this is a tag with no text content. If it contains no text or only <br> elements, remove it.
			var to_remove = true;
			for (var j=dom_elem.children.length-1; j>=0; j--) {
				if (! (dom_elem.children[j] instanceof HTMLBRElement)) {
					//console.log('DEBUG found something otherthan <br>');
					//console.log(dom_elem.children[j]);
					to_remove = false;
					break;
				}
			}
			if (to_remove) {
				removeNode(dom_elem);
			}
		}
	} //removeEmptyElementDescendants()

	/**
	 * Checks for self-closing elements
	 * @param: {HTMLElement}
	 * @return {bool}
	 */
	function isSelfClosingElement(dom_elem) {
		if (typeof dom_elem !== 'object' || !(dom_elem instanceof HTMLElement)) {
			return false;
		}
		var dom_elem_clone = document.createElement(dom_elem.tagName);
		return dom_elem_clone.outerHTML.indexOf( "><" ) == -1;
	}
	
	/**
	 * Locks scroll position of an element, and removes scrollbars
	 * - Note: if already locked, operation will fail
	 * @param: {HTMLElement}
	 * @return: {bool} success
	 */
	function lockScroll(elem) {
		// Always work on body instead of html
		if (elem === document.scrollingElement) {
			elem = document.body;
		}

		var state = elem.getAttribute('data-locked-scroll');
		if (state === 'locked') {
			//console.log('Already locked');
			return false;
		}
		
		// Remember scroll position
		elem.setAttribute('data-locked-scroll-left-stored-by-domo', elem.scrollLeft);
		elem.setAttribute('data-locked-scroll-top-stored-by-domo', elem.scrollTop);
		elem.setAttribute('data-locked-scroll-stored-by-domo', 'locked');
		
		// Remember overflow setting
		var cs = window.getComputedStyle(elem, null);
		elem.setAttribute('data-previous-overflow-x-stored-by-domo', cs.overflowX);
		elem.setAttribute('data-previous-overflow-y-stored-by-domo', cs.overflowY);
		
		// Set elem overflow
		elem.style.overflowX = 'hidden';
		elem.style.overflowY = 'hidden';
		
		return true;
	} //lockScroll()
	
	/**
	 * Unlocks an element that was locked by lockScroll()
	 * @param: {HTMLElement}
	 * @return: {boolean} success
	 */
	function unlockScroll(elem) {
		// Always work on body instead of html
		if (elem === document.scrollingElement) {
			elem = document.body;
		}

		var state = elem.getAttribute('data-locked-scroll-stored-by-domo');
		if (state !== 'locked') {
			//console.log('Not locked');
			return false;
		}
		
		// Recall overflow settings
		elem.style.overflowX = elem.getAttribute('data-previous-overflow-x-stored-by-domo');
		elem.style.overflowY = elem.getAttribute('data-previous-overflow-y-stored-by-domo');
		elem.removeAttribute('data-previous-overflow-x-stored-by-domo');
		elem.removeAttribute('data-previous-overflow-y-stored-by-domo');
		// DEBUG
		//console.log('Reset overflowX: ' + elem.style.overflowX);
		//console.log('Reset overflowX: ' + elem.style.overflowY);
		
		// Recall scroll position
		elem.scrollLeft = elem.getAttribute('data-locked-scroll-left-stored-by-domo');
		elem.scrollTop = elem.getAttribute('data-locked-scroll-top-stored-by-domo');
		elem.removeAttribute('data-locked-scroll-top-stored-by-domo');
		elem.removeAttribute('data-locked-scroll-left-stored-by-domo');
		elem.removeAttribute('data-locked-scroll-stored-by-domo');
		
		return true;
	} //unlockScroll

	/**
	 * Checks if element's scroll has been locked by lockScroll()
	 * @param: {HTMLElement}
	 * @return: {bool}
	 */
	function isLockedScroll(elem) {
		// Always work on body instead of html
		if (elem === document.scrollingElement) {
			elem = document.body;
		}

		var state = elem.getAttribute('data-locked-scroll');
		return (state === 'locked');
	} //isLockedScroll()
	
	/**
	 * Gets position of element w.r.t. screen, i.e. does not include scrollTop & scrollLeft amounts.
	 * @param: {HTMLElement} elem
	 * @return: {object} point coordinates, e.g. {x:1, y:2}
	 */
	function getPositionOnScreen(elem) {
		if (typeof elem !== 'object' || !(elem instanceof HTMLElement)) {
			throw new TypeError('getPositionOnScreen() - param is not an HTMLElement. (' + typeof elem + ')');
		}
		
		var elem_rect = elem.getBoundingClientRect();
		var offsetTop = elem_rect.top - document.documentElement.clientTop;
		var offsetLeft = elem_rect.left - document.documentElement.clientLeft;

		return {x:offsetLeft, y:offsetTop};
	} // getPositionOnScreen()

	/**
	 * Gets position of element w.r.t. page, i.e. position off the top left corner of the document
	 */
	function getPositionOnPage(elem) {
		if (typeof elem !== 'object' || !(elem instanceof HTMLElement)) {
			throw new TypeError('getPositionOnPage() - param is not an HTMLElement. (' + typeof elem + ')');
		}

		var x = 0, y = 0;
		do {
			y += elem.offsetTop  || 0;
			x += elem.offsetLeft || 0;
			elem = elem.offsetParent;
		} while(elem);

		return {x:x, y:y};
	} // getPositionOnPage()

	/**
	 * @param: {HTMLElement} dom_elem
	 *         {object} options
	 *            {number} scrollLeft  (px)
     *            {number} scrollTop   (px)
     *            {number} duration    (ms)
     *            {string} easing      name of easing function to use, e.g. "linear", "easeInOutQuad"
     * @return: {Promise} resolves when completed. Never rejects.
	 */
	function animateScroll(dom_elem, options) {
		// t: current time, b: beginning value, c: change in value, d: duration
		var EASING_FUNCTIONS = {
			'linear': function(t, b, c, d) {return c * t / d + b;},
			'easeInQuad': function(t, b, c, d) {t /= d; return c*t*t + b;},
			'easeOutQuad': function(t, b, c, d) {t /= d; return -c * t*(t-2) + b;},
			'easeInOutQuad': function(t, b, c, d) {t/= d/2;if (t < 1) return c/2*t*t + b; t--; return -c/2 * (t*(t-2) - 1) + b;},
			'easeInCubic': function (t, b, c, d) {t /= d; return c*t*t*t + b;},
			'easeOutCubic': function (t, b, c, d) {t /= d; t--;return c*(t*t*t + 1) + b;},
			'easeInOutCubic': function (t, b, c, d) {t /= d/2; if (t < 1) return c/2*t*t*t + b; t -= 2; return c/2*(t*t*t + 2) + b;},
			'easeInQuart': function (t, b, c, d) {t /= d;return c*t*t*t*t + b;},
			'easeOutQuart': function (t, b, c, d) {t /= d;t--;return -c * (t*t*t*t - 1) + b;},
			'easeInOutQuart': function (t, b, c, d) {t /= d/2;if (t < 1) return c/2*t*t*t*t + b;t -= 2;return -c/2 * (t*t*t*t - 2) + b;},
			'easeInQuint': function (t, b, c, d) {t /= d;return c*t*t*t*t*t + b;},
			'easeOutQuint': function (t, b, c, d) {t /= d;t--;return c*(t*t*t*t*t + 1) + b;},
			'easeInOutQuint': function (t, b, c, d) {t /= d/2;if (t < 1) return c/2*t*t*t*t*t + b;t -= 2;return c/2*(t*t*t*t*t + 2) + b;},
			'easeInSine': function (t, b, c, d) {return -c * Math.cos(t/d * (Math.PI/2)) + c + b;},
			'easeOutSine': function (t, b, c, d) {return c * Math.sin(t/d * (Math.PI/2)) + b;},
			'easeInOutSine': function (t, b, c, d) {return -c/2 * (Math.cos(Math.PI*t/d) - 1) + b;},
			'easeInExpo': function (t, b, c, d) {return c * Math.pow( 2, 10 * (t/d - 1) ) + b;},
			'easeOutExpo': function (t, b, c, d) {return c * ( -Math.pow( 2, -10 * t/d ) + 1 ) + b;},
			'easeInOutExpo': function (t, b, c, d) {t /= d/2;if (t < 1) return c/2 * Math.pow( 2, 10 * (t - 1) ) + b;t--;return c/2 * ( -Math.pow( 2, -10 * t) + 2 ) + b;},
			'easeInCirc': function (t, b, c, d) {t /= d;return -c * (Math.sqrt(1 - t*t) - 1) + b;},
			'easeOutCirc': function (t, b, c, d) {t /= d;t--;return c * Math.sqrt(1 - t*t) + b;},
			'easeInOutCirc': function (t, b, c, d) {t /= d/2;if (t < 1) return -c/2 * (Math.sqrt(1 - t*t) - 1) + b;t -= 2;return c/2 * (Math.sqrt(1 - t*t) + 1) + b;},
			'easeInElastic': function(t, b, c, d) {var ts=(t/=d)*t;var tc=ts*t;return b+c*(56*tc*ts + -105*ts*ts + 60*tc + -10*ts);},
			'easeOutElastic': function(t, b, c, d) {var ts=(t/=d)*t;var tc=ts*t;return b+c*(56*tc*ts + -175*ts*ts + 200*tc + -100*ts + 20*t);},
			'easeInOutElastic': function(t, b, c, d){if (t < d/2) return EASING_FUNCTIONS.easeInElastic (t*2, 0, c, d) * .5 + b; return EASING_FUNCTIONS.easeOutElastic (t*2-d, 0, c, d) * .5 + c*.5 + b;},
			'easeInBack': function (t, b, c, d) {var s = 1.70158; return c*(t/=d)*t*((s+1)*t - s) + b;},
			'easeOutBack': function (t, b, c, d) {var s = 1.70158; return c*((t=t/d-1)*t*((s+1)*t + s) + 1) + b;},
			'easeInOutBack': function (t, b, c, d) {var s = 1.70158; t /= d/2; if (t < 1) return c/2*(t*t*(((s*=(1.525))+1)*t - s)) + b; return c/2*((t-=2)*t*(((s*=(1.525))+1)*t + s) + 2) + b;},
			'easeOutBounce': function (t, b, c, d) {return(t/=d)<1/2.75?7.5625*c*t*t+b:t<2/2.75?c*(7.5625*(t-=1.5/2.75)*t+.75)+b:t<2.5/2.75?c*(7.5625*(t-=2.25/2.75)*t+.9375)+b:c*(7.5625*(t-=2.625/2.75)*t+.984375)+b},
			'easeInBounce': function(t, b, c, d) {return c - EASING_FUNCTIONS.easeOutBounce(d-t, 0, c, d) + b;},
			'easeInOutBounce': function (t, b, c, d) {if (t < d/2) return EASING_FUNCTIONS.easeInBounce (t*2, 0, c, d) * .5 + b; return EASING_FUNCTIONS.easeOutBounce (t*2-d, 0, c, d) * .5 + c*.5 + b;}
		};

		// Clone options
		var DEFAULT_OPTIONS = {
			'scrollLeft': 0,
			'scrollTop': 0,
			'duration': 200,
			'easing': 'easeInOutQuad'
		};
		options = Object.assign({}, DEFAULT_OPTIONS, options);

		// Process options: duration
		if (typeof options['duration'] !== 'number') {
			options['duration'] = DEFAULT_OPTIONS['duration'];
		}
		if (options['duration'] === 0) {
			options['duration'] = 1; // prevent any potential divide-by-zero errors
		}

		// Process options: easing
		if (typeof options['easing'] === 'string') {
			// Ensure user-specified easing exists, else use default.
			if (options['easing'] in EASING_FUNCTIONS) {
				options['easing'] = EASING_FUNCTIONS[options['easing']];
			}
			else {
				console.warn('Unknown eaasing ' + options['easing'] + '. Easing function defaulting to ' + DEFAULT_OPTIONS['easing']);
				options['easing'] = EASING_FUNCTIONS[DEFAULT_OPTIONS['easing']];
			}
		}

		// State
		var start_time; // to start on first frame
		var start_values = {
			'scrollLeft': dom_elem.scrollLeft,
			'scrollTop': dom_elem.scrollTop
		};
		var change_values = {
			'scrollLeft': options['scrollLeft'] - start_values['scrollLeft'],
			'scrollTop': options['scrollTop'] - start_values['scrollTop']
		};

		return new Promise(function(resolve) {
			// Needs to be inner function as we need to resolve
			function step(timestamp) {
				if (!start_time) {
					start_time = timestamp;
				}

				var elapsed_duration = timestamp - start_time;
				
				// Render the new position
	    		if (elapsed_duration < options['duration']) {
					dom_elem.scrollLeft = options['easing'](elapsed_duration, start_values['scrollLeft'], change_values['scrollLeft'], options['duration']); // t, b, c, d;
	    			dom_elem.scrollTop = options['easing'](elapsed_duration, start_values['scrollTop'], change_values['scrollTop'], options['duration']); // t, b, c, d;

	      			window.requestAnimationFrame(step);
	    		}
	    		else {
	    			dom_elem.scrollLeft = options['scrollLeft'];
	    			dom_elem.scrollTop = options['scrollTop'];
	    			
	    			resolve();
	    		} // Last Frame
			} // step()

			window.requestAnimationFrame(step); // note: requestAnimationFrame calls with a timestamp (ms) as param
		});
	} //animateScroll()

	/**
	 * Populates a <select> with children <option> elements using an array of object literals.
	 * - Note: any existing options in the select will be wiped out
	 * @param: {HTMLSelectElement} dom_select
	 *         {array} options -  objects in the format of {"value": "HTML Text"}
	 * @return: {number} count of options added
	 */
	function populateSelect(dom_select, options) {
		// Check params
		if (! (dom_select instanceof HTMLSelectElement)) {
			throw new TypeError('populateSelect() - param is not an HTMLSelectElement.  (' + typeof dom_select + ')');
		}
		if (! (options instanceof Array)) {
			throw new TypeError('populateSelect() - options is not an Array. (' + typeof options +')');
		}

		// Do work
		var count = 0;
		var html = '';
		for (var i=0,len=options.length; i<len; i++) {
			if (typeof options[i] !== 'object') {
				continue;
			}

			var value = Object.keys(options[i])[0];
			var text = options[i][value];
			html += '<option value="'+value+'">' + text + '</option>';
			count++;
		}
		dom_select.innerHTML = html;

		return count;
	} // populateSelect()

	/**
	 * Populates a <datalist> with children <option> elements using an array of strings
	 * - Note: any existing options in the datalist will be wiped out
	 * @param: {HTMLDataListElement} dom_datalist
	 *         {array} options - strings
	 * @return: {number} count of options added
	 */
	function populateDatalist(dom_datalist, options) {
		// Check params
		if (! (dom_datalist instanceof HTMLDataListElement)) {
			throw new TypeError('populateDatalist() - param is not an HTMLDataListElement.  (' + typeof dom_datalist + ')');
		}
		if (! (options instanceof Array)) {
			throw new TypeError('populateDatalist() - options is not an Array. (' + typeof options +')');
		}

		// Do work
		var count = 0;
		var html = '';
		for (var i=0,len=options.length; i<len; i++) {
			if (typeof options[i] !== 'string') {
				continue;
			}

			html += '<option value="'+options[i]+'">';
			count++;
		}
		dom_datalist.innerHTML = html;

		return count;
	} // populateDatalist()

	/**
	 * Creates checkbox children using an array of object literals.
	 * - Note: any existing HTML in the div will be wiped out
	 * @param: {HTMLElement} dom_div - container for generated checkboxes
	 *         {array} options - objects in the format of {"value": "HTML Text"}
	 *         {string} name - name attribute of the checkboxes. Optional. (DEFAULTS to "checkbox")
	 * @return: {number} count of checkboxes added
	 */
	function populateCheckboxes(dom_div, options, name) {
		// Check params
		if (! (dom_div instanceof HTMLElement)) {
			throw new TypeError('populateCheckboxes() - param is not an HTMLElement.  (' + typeof dom_div + ')');
		}
		if (! (options instanceof Array)) {
			throw new TypeError('populateCheckboxes() - options is not an Array. (' + typeof options +')');
		}
		if (typeof name !== 'string') {
			name = 'checkbox';
		}

		// Do work
		var count = 0;
		var html = '';
		for (var i=0,len=options.length; i<len; i++) {
			if (typeof options[i] !== 'object') {
				continue;
			}

			var value = Object.keys(options[i])[0];
			var text = options[i][value];
			var id = name + '-' + checkbox_radio_counter++;

			html += '' +
				'<input type="checkbox" id="' + id + '" name="' + name + '" value="' + value + '" />' +
				'<label for="' + id + '">' + text + '</label>';
			count++;
		}
		dom_div.innerHTML = html;

		return count;
	} //populateCheckboxes()

	/**
	 * Creates radio children using an array of object literals.
	 * - Note: any existing HTML in the div will be wiped out
	 * - Note: the first radio will be checked
	 * @param: {HTMLElement} dom_div - container for generated radios
	 *         {array} options - objects in the format of {"value": "Text to show"}
	 *         {string} name - name attribute of the radios. Optional. (DEFAULTS to "radio")
	 * @return: {number} count of radios added
	 */
	function populateRadios(dom_div, options, name) {
		// Check params
		if (! (dom_div instanceof HTMLElement)) {
			throw new TypeError('populateRadios() - param is not an HTMLElement. (' + typeof dom_div + ')');
		}
		if (! (options instanceof Array)) {
			throw new TypeError('populateRadios() - options is not an Array. (' + typeof options +')');
		}
		if (typeof name !== 'string') {
			name = 'radio';
		}

		// Do work
		var count = 0;
		var html = '';
		for (var i=0,len=options.length; i<len; i++) {
			if (typeof options[i] !== 'object') {
				continue;
			}

			var value = Object.keys(options[i])[0];
			var text = options[i][value];
			var id = name + '-' + checkbox_radio_counter++;

			var checked_text = '';
			if (i===0) {
				checked_text = 'checked="checked"';	
			}
			
			html += '' +
				'<input type="radio" id="' + id + '" name="' + name + '" value="' + value + '"' + checked_text + ' />' +
				'<label for="' + id + '">' + text + '</label>';
			count++;
		}
		dom_div.innerHTML = html;

		return count;
	} //populateRadios()

	/**
	 * Removes options from a select / datalist
	 * @param: {HTMLSelectElement | HTMLDataListElement}
	 *         {string} value of the option
	 */
	function removeOption(dom_elem, value) {
		if (! (dom_elem instanceof HTMLElement)) {
			throw new TypeError('removeOption() - param is not an HTMLElement. (' + typeof dom_elem + ')');
		}

		for (var i = dom_elem.length - 1; i >= 0; --i) {
			if (dom_elem[i].value === value) {
				dom_elem.removeChild(dom_elem[i]);
		    }
		}
	} // removeOption()

	/**
	 * Removes a checkbox from a div of checkboxes
	 *  - Note: checkboxes NEED to have a <label> immediately after <input>.
	 * @param: {HTMLElement} dom_div - container of checkboxes
	 *         {string} value of the checkbox
	 */
	function removeCheckbox(dom_div, value) {
		// Check params
		if (! (dom_div instanceof HTMLElement)) {
			throw new TypeError('removeCheckbox() - param is not an HTMLElement. (' + typeof dom_div + ')');
		}

		var dom_checkboxes = [].slice.call(dom_div.querySelectorAll('input[type="checkbox"]'));

		for (var i = dom_checkboxes.length - 1; i >= 0; --i) {

			if (dom_checkboxes[i].value === value) {
				var dom_parent = dom_checkboxes[i].parentNode;	
				var dom_label = dom_checkboxes[i].nextElementSibling;
				dom_parent.removeChild(dom_checkboxes[i]);
				dom_parent.removeChild(dom_label);
		    }
		}
	} // removeCheckbox()

	/**
	 * Removes a radio from a div of radios
	 *  - Note: checkboxes NEED to have a <label> immediately after <input>.
	 * @param: {HTMLElement} dom_div - container of radios
	 *         {string} value of the radio
	 */
	function removeRadio(dom_div, value) {
		// Check params
		if (! (dom_div instanceof HTMLElement)) {
			throw new TypeError('removeRadio() - param is not an HTMLElement. (' + typeof dom_div + ')');
		}

		var dom_checkboxes = [].slice.call(dom_div.querySelectorAll('input[type="radio"]'));

		for (var i = dom_checkboxes.length - 1; i >= 0; --i) {

			if (dom_checkboxes[i].value === value) {
				var dom_parent = dom_checkboxes[i].parentNode;	
				var dom_label = dom_checkboxes[i].nextElementSibling;
				dom_parent.removeChild(dom_checkboxes[i]);
				dom_parent.removeChild(dom_label);
		    }
		}
	} // removeRadio()

	/**
	 * Adds a CSS class to a form element and its associated label(s) and output(s).
	 * @param: {HTMLInputElement / HTMLTextAreaElement / HTMLSelectElememt} 
	 *         {string} css_class
	 */ 
	function addClassToGroup(dom_elem, css_class) {
		var i,len;
		var id = dom_elem.id;
		if (id) {
			var dom_labels = document.querySelectorAll('label[for="'+id+'"]');

			for (i=0,len=dom_labels.length; i<len; i++) {
				dom_labels[i].classList.add(css_class);
			}

			var dom_outputs = document.querySelectorAll('output[for="'+id+'"]');
			for (i=0,len=dom_outputs.length; i<len; i++) {
				dom_outputs[i].classList.add(css_class);
			}			
		}

		dom_elem.classList.add(css_class);
	} // addClassToGroup()

	function removeClassFromGroup(dom_elem, css_class) {
		var i,len;
		var id = dom_elem.id;
		if (id) {
			var dom_labels = document.querySelectorAll('label[for="'+id+'"]');

			for (i=0,len=dom_labels.length; i<len; i++) {
				dom_labels[i].classList.remove(css_class);
			}

			var dom_outputs = document.querySelectorAll('output[for="'+id+'"]');
			for (i=0,len=dom_outputs.length; i<len; i++) {
				dom_outputs[i].classList.remove(css_class);
			}			
		}

		dom_elem.classList.remove(css_class);
	} // removeClassFromGroup()

	/**
	 * - makes all <input> and <textarea> in the form READONLY
	 * - disables all options other than the current selected one in all <select>
	 * - disables all <button type="submit"> within form
	 * @param: {HTMLFormEleemnt}
	 */
	function disableForm(dom_form) {
		if (! (dom_form instanceof HTMLFormElement)) {
			throw new TypeError('disableForm() - param is not HTMLFormElement. (' + typeof dom_div + ')');
		}

		// Don't double disable
		if (isDisabledForm(dom_form)) {
			return;
		}

		var dom_elem, node_name;
		for (var i=0, len=dom_form.elements.length; i<len; i++) {
			dom_elem = dom_form.elements[i];
			node_name = dom_elem.nodeName.toUpperCase();

			if (node_name === 'INPUT' || node_name === 'TEXTAREA') {
				dom_elem.setAttribute('data-orig-readonly', dom_elem.readOnly);
				dom_elem.readOnly = true;
			}
			else if (node_name === 'SELECT') {
				for (var j=0, lenj=dom_elem.options.length; j<lenj; j++) {
					if (j === dom_elem.selectedIndex) {
						continue;
					}
					dom_elem.options[j].setAttribute('data-orig-disabled', dom_elem.options[j].disabled);
					dom_elem.options[j].disabled = true;
				}
			}
		} // for each element

		var dom_submit_buttons = dom_form.querySelectorAll('button[type=submit]');
		for (i=0, len=dom_submit_buttons.length; i<len; i++) {
			dom_submit_buttons[i].disabled = true;
		}

		dom_form.setAttribute('data-disabled', 'true');
	} //disableForm()

	/**
	 * Undos all effects done by disableForm()
	 * @param: {HTMLFormEleemnt}
	 */
	function enableForm(dom_form) {
		if (! (dom_form instanceof HTMLFormElement)) {
			throw new TypeError('enableForm() - param is not HTMLFormElement. (' + typeof dom_div + ')');
		}

		// Don't handle forms that weren't disabled
		if (!isDisabledForm(dom_form)) {
			return;
		}

		var dom_elem, node_name;
		for (var i=0, len=dom_form.elements.length; i<len; i++) {
			dom_elem = dom_form.elements[i];
			node_name = dom_elem.nodeName.toUpperCase();

			if (node_name === 'INPUT' || node_name === 'TEXTAREA') {
				var orig_readonly = dom_elem.getAttribute('data-orig-readonly');
				dom_elem.removeAttribute('data-orig-readonly');
				dom_elem.readOnly = (orig_readonly === 'true');
			}
			else if (node_name === 'SELECT') {
				for (var j=0, lenj=dom_elem.options.length; j<lenj; j++) {
					var orig_disabled = dom_elem.options[j].getAttribute('orig-disabled');
					dom_elem.options[j].removeAttribute('orig-disabled');
					dom_elem.options[j].disabled = (orig_disabled === 'true');
				}
			}
		} // for each element
		
		var dom_submit_buttons = dom_form.querySelectorAll('button[type=submit]');
		for (i=0, len=dom_submit_buttons.length; i<len; i++) {
			dom_submit_buttons[i].disabled = false;
		}

		dom_form.removeAttribute('data-disabled');
	} //enableForm()

	/**
	 * Checks if a form has been disabled through disableForm()
	 * @param: {HTMLFormEleemnt}
	 * @return: {bool}
	 */
	function isDisabledForm(dom_form) {
		if (! (dom_form instanceof HTMLFormElement)) {
			throw new TypeError('isDisabledForm() - param is not HTMLFormElement. (' + typeof dom_div + ')');
		}

		var disabled = dom_form.getAttribute('data-disabled');
		return (disabled === 'true');
	} //isDisabledForm()
	
	global.domo = {
		/* DOM functions */
		'getMeta': getMeta,
		'setMeta': setMeta,
		'addCSS': addCSS,
		'addJS': addJS,
		'removeCSS': removeCSS,
		'removeCSSByData': removeCSSByData,
		'removeJS': removeJS,
		'removeJSByData': removeJSByData,
		'str2dom': str2dom,
		'getSelectedText': getSelectedText,

		/* DOM element functions */
		'selectText': selectText,
		'elem2csv': elem2csv,
		'wrapNode': wrapNode,
		'removeNode': removeNode,
		'removeBrChildren': removeBrChildren,
		'removeEmptyElementDescendants': removeEmptyElementDescendants,
		'isSelfClosingElement': isSelfClosingElement,
		'lockScroll': lockScroll,
		'unlockScroll': unlockScroll,
		'isLockedScroll': isLockedScroll,
		'getPositionOnScreen': getPositionOnScreen,
		'getPositionOnPage': getPositionOnPage,
		'animateScroll': animateScroll,

		/* Form functions */
		'populateSelect': populateSelect,
		'populateDatalist': populateDatalist,
		'populateCheckboxes': populateCheckboxes,
		'populateRadios': populateRadios,
		'removeOption': removeOption,
		'removeCheckbox': removeCheckbox,
		'removeRadio': removeRadio,
		'addClassToGroup': addClassToGroup,
		'removeClassFromGroup': removeClassFromGroup,
		'enableForm': enableForm,
		'disableForm': disableForm,
		'isDisabledForm': isDisabledForm
	};
})(this, window, document);
