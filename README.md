# Domo #

A JavaScript library to make operations on DOM elements easier.

### Set Up ###

1. Download the latest release (as of 21 Dec 2019, v3.0.0).
2. Include in your HTML (preferably just before `</body>`):

    `<script src="domo-3.0.0.js"></script>`
	
3. Also consider including polyfills to normalize your target environments: [Fullscreen API](https://github.com/neovov/Fullscreen-API-Polyfill), [document.scrollingElement](https://github.com/mathiasbynens/document.scrollingElement)

### Browser Support ###
* Chrome
* Edge
* Firefox
* Opera
* Safari

### API ###
**getMeta**

```
/**
 * Returns the content of a meta tag by its name
 * @param: {string} name
 * @return: {string | null}
 */
 
// Sample usage
var description = domo.getMeta('desciption');
```

**setMeta**

```
/**
 * Sets a meta in <head>. If meta does not exist, set it.
 * Note: TODO...
 * @param: {string} name
 *         {string} content
 * @return: {HTMLMetaElement}
 */
 
// Sample usage
var dom_meta = domo.setMeta('desciption', 'This is an awesome web page');
```

**addCSS**

```
/**
 * @param: {string} file path
 *         {object} data (optional) - key-value pairs of HTML5 data attributes
 * @return: {HTMLLinkElement}
 */

// Sample usage
var dom_link = domo.addCSS('css/home.css', {page: 'home'});
```

**removeCSS**

```
/**
 * @param: {string} file path
 * @return: {number} of link tags removed
 */

// Sample usage
var removed_count = domo.removeCSS('css/home.css');
```

**addJS**

```
/**
 * @param: {string} file path
 *         {object} data (optional) - key-value pairs of HTML5 data attributes
 * @return: {HTMLScriptElement}
 */
 
// Sample usage
var dom_script = domo.addJS('js/home.js', {page: 'home'});
```

**removeJS**

```
/**
 * @param: {string} file path
 * @return: {number} of script tags removed
 */

// Sample usage
var removed_count = domo.removeJS('js/home.js');
```

**getSelectedText**

```
/**
 * Gets the text currently selected (highlighted)
 * @return: {string}
 */

// Sample Usage
var text = domo.getSelectedText(); // whatever the user has highlighted will be now stored in text (as a string)
```

**selectText**

```
/**
 * Selects all text in an element (akin to highlighting with your mouse)
 * @param: {HTMLElement}
 */

// Sample usage
var some_div = document.getElementById('some-div');
domo.selectText(some_div); // div is now selected (mouse-highlighted)
```

**lockScroll**

```
/**
 * Locks scroll position of an element, and removes scrollbars
 * - Note: if already locked, operation will fail
 * @param: {HTMLElement}
 * @return: {bool} success
 */

// Sample usage
var dom_my_div = document.getElementById('my-div');
domo.lockScroll(dom_my_div);
```

**unlockScroll**

```
/**
 * Unlocks an element that was locked by lockScroll()
 * @param: {HTMLElement}
 * @return: {boolean} success
 */

// Sample usage
var dom_my_div = document.getElementById('my-div');
if (domo.lockScroll(dom_my_div)) {
	console.log('Successfully unlocked');
}
```

**getPositionOnScreen**

```
/**
 * Gets position of element w.r.t. screen, i.e. ignoring scroll values
 * @param: {HTMLElement} elem
 * @return: {object} point coordinates, e.g. {x:1, y:2}
 */
 
// Sample usage
var dom_my_div = document.getElementById('my-div');
var pos = domo.getPositionOnScreen(dom_my_div);
console.log('My div is at ' + pos.x + ', ' + pos.y);
```

**populateSelect**

```
/**
 * Populates a <select> with children <option> elements using an object literal.
 * - Note: any existing options in the select will be wiped out
 * @param: {HTMLSelectElement}
 *         {object literal} options in the format of {"value": "HTML Text"}
 */

// Sample usage
var dom_select = document.getElementById('my-select');
var options = {
	'1': 'Option One',
	'2': 'Option Two',
	'3': 'Option Three'
}
domo.populateSelect(dom_select, options);
```

**populateCheckboxes**

```
/**
 * Creates checkbox children using an object literal
 * - Note: any existing HTML in the div will be wiped out
 * @param: {HTMLElement} div that is going to contain the checkboxes
 *         {object literal} options in the format of "value": "Text to show"
 *         {string} name of the checkboxes - optional. (DEFAULTS to "checkbox")
 */

// Sample usage
var dom_div = document.getElementById('checkbox-container');
var options = {
	'1': 'Box One',
	'2': 'Box Two',
	'3': 'Box Three'
}
domo.populateCheckboxes(dom_div, options);
```

**populateRadios**

```
/**
 * Creates radio children using an object literal
 * - Note: any existing HTML in the div will be wiped out
 * @param: {HTMLElement} div that is going to contain the radios
 *         {object literal} options in the format of "value": "Text to show"
 *         {string} name of the radios - optional. (DEFAULTS to "radio")
 */

// Sample usage
var dom_div = document.getElementById('radio-container');
var options = {
	'1': 'Box One',
	'2': 'Box Two',
	'3': 'Box Three'
}
domo.populateRadios(dom_div, options);
```

**elem2csv**

```
/**
 * Converts Table or Form element into a CSV.
 * - Note for tables:
 *       if parseHeader is false, rows in <thead> will be skipped.
 *       <thead> is expected to occur at most once, and only at the beginning.
 * - Note for forms:
 *       if parseHeader is true, returns a line containing all names/IDs, and a line containing all values.
 *       if praseHeader is false, returns a single-line CSV of values only.
 *
 * @param: {HTMLTableElement | HTMLFormElement} elem
 *         {object} options (optional)
 *                  - {string} delimiter (defaults to comma. Must be 1 char)
 *                  - {string} lineEnding (defaults to \n. Max of 2 chars)
 *                  - {string} quote {defaults to ". Must be 1 char)
 *                  - {boolean} parseHeader (defaults to true)
 *                  - {boolean} trim (defaults to true)
 *                  - {boolean} unescapeHTML (defaults to true for table elements, false otherwise)
 * @return: {string}
 */

// Sample usage
var csv = domo.elem2csv(dom_my_table);
```

**str2dom**

```
/**
 * Converts a string containing HTML into DOM Elements
 * @param: {string}
 * @return: {Array} of HTMLElement
 */

// Sample usage
var html = '<h1>Hello</h1><p>Lorem ipsum</p>';
var elems = domo.str2dom(html); // elems is now array of 2 HTMLElements - one H1, one P
```

**requestFullScreen**

```
/**
 * Requests full screen using FullScreen API. Useful for mobile.
 * - Note: must be called only as a result of a user-generated event
 * - Note: document must allow fullscreen, e.g. not enabled for iframes without "allowfullscreen" attribute
 * - Note: requires browser support of fullscreen API
 * @param: {HTMLElement} elem - default: documentElement
 * @return: {bool} success
 */

// Sample usage
var some_div = document.getElementById('some-div');
domo.requestFullScreen(some_div); // div is now full screen
```
### FAQ ###

1. What is the use of setting meta tags after the page is loaded?
> For most cases, setting meta tags is redundant since the users don't see them.
>
> However, some browsers make use of the `desciption` meta, so setting them dynamically make sense for AJAX-loaded pages.
> The Facebook plugin also makes use of some meta tags, e.g. when sharing. Setting them dynamically prior to calling Facebook's API can be useful.

1. Why is there no method to help get/set title?
> These can be done simply by accessing `document.title`, which is clean and simple enough. In this case, we think trying to be comprehensive is counter-productive.
