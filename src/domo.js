/***
 * Library - Domo
 *
 * Useful methods for DOM elements
 ***/
(function(global, window, document) { // eslint-disable-line no-shadow
	'use strict';

	/* eslint-disable */
	// settings
	const LOCK_SCROLL_STATE_DATA_ATTRIBUTE = 'data-locked-scroll-stored-by-domo';
	const LOCK_SCROLL_SCROLL_LEFT_DATA_ATTRIBUTE = 'data-locked-scroll-left-stored-by-domo';
	const LOCK_SCROLL_SCROLL_TOP_DATA_ATTRIBUTE = 'data-locked-scroll-top-stored-by-domo';
	const LOCK_SCROLL_OVERFLOW_X_DATA_ATTRIBUTE = 'data-previous-overflow-x-stored-by-domo';
	const LOCK_SCROLL_OVERFLOW_Y_DATA_ATTRIBUTE = 'data-previous-overflow-y-stored-by-domo';
	const ANIMATION_EASING_FUNCTIONS = { // t: current time, b: beginning value, c: change in value, d: duration
		'linear': function(t, b, c, d) {return c * t / d + b;},
		'easeInQuad': function(t, b, c, d) {t /= d; return c*t*t + b;},
		'easeOutQuad': function(t, b, c, d) {t /= d; return -c * t*(t-2) + b;},
		'easeInOutQuad': function(t, b, c, d) {t/= d/2;if (t < 1) {return c/2*t*t + b;} t--; return -c/2 * (t*(t-2) - 1) + b;},
		'easeInCubic': function (t, b, c, d) {t /= d; return c*t*t*t + b;},
		'easeOutCubic': function (t, b, c, d) {t /= d; t--;return c*(t*t*t + 1) + b;},
		'easeInOutCubic': function (t, b, c, d) {t /= d/2; if (t < 1) {return c/2*t*t*t + b;} t -= 2; return c/2*(t*t*t + 2) + b;},
		'easeInQuart': function (t, b, c, d) {t /= d;return c*t*t*t*t + b;},
		'easeOutQuart': function (t, b, c, d) {t /= d;t--;return -c * (t*t*t*t - 1) + b;},
		'easeInOutQuart': function (t, b, c, d) {t /= d/2;if (t < 1) {return c/2*t*t*t*t + b;} t -= 2; return -c/2 * (t*t*t*t - 2) + b;},
		'easeInQuint': function (t, b, c, d) {t /= d;return c*t*t*t*t*t + b;},
		'easeOutQuint': function (t, b, c, d) {t /= d;t--;return c*(t*t*t*t*t + 1) + b;},
		'easeInOutQuint': function (t, b, c, d) {t /= d/2;if (t < 1) {return c/2*t*t*t*t*t + b;} t -= 2; return c/2*(t*t*t*t*t + 2) + b;},
		'easeInSine': function (t, b, c, d) {return -c * Math.cos(t/d * (Math.PI/2)) + c + b;},
		'easeOutSine': function (t, b, c, d) {return c * Math.sin(t/d * (Math.PI/2)) + b;},
		'easeInOutSine': function (t, b, c, d) {return -c/2 * (Math.cos(Math.PI*t/d) - 1) + b;},
		'easeInExpo': function (t, b, c, d) {return c * Math.pow( 2, 10 * (t/d - 1) ) + b;},
		'easeOutExpo': function (t, b, c, d) {return c * ( -Math.pow( 2, -10 * t/d ) + 1 ) + b;},
		'easeInOutExpo': function (t, b, c, d) {t /= d/2;if (t < 1) {return c/2 * Math.pow( 2, 10 * (t - 1) ) + b;} t--; return c/2 * ( -Math.pow( 2, -10 * t) + 2 ) + b;},
		'easeInCirc': function (t, b, c, d) {t /= d;return -c * (Math.sqrt(1 - t*t) - 1) + b;},
		'easeOutCirc': function (t, b, c, d) {t /= d;t--;return c * Math.sqrt(1 - t*t) + b;},
		'easeInOutCirc': function (t, b, c, d) {t /= d/2;if (t < 1) {return -c/2 * (Math.sqrt(1 - t*t) - 1) + b;} t -= 2; return c/2 * (Math.sqrt(1 - t*t) + 1) + b;},
		'easeInElastic': function(t, b, c, d) {let ts=(t/=d)*t;let tc=ts*t;return b+c*(56*tc*ts + -105*ts*ts + 60*tc + -10*ts);},
		'easeOutElastic': function(t, b, c, d) {let ts=(t/=d)*t;let tc=ts*t;return b+c*(56*tc*ts + -175*ts*ts + 200*tc + -100*ts + 20*t);},
		'easeInOutElastic': function(t, b, c, d){if (t < d/2) {return ANIMATION_EASING_FUNCTIONS.easeInElastic (t*2, 0, c, d) * 0.5 + b;} return ANIMATION_EASING_FUNCTIONS.easeOutElastic (t*2-d, 0, c, d) * 0.5 + c*0.5 + b;},
		'easeInBack': function (t, b, c, d) {let s = 1.70158; return c*(t/=d)*t*((s+1)*t - s) + b;},
		'easeOutBack': function (t, b, c, d) {let s = 1.70158; return c*((t=t/d-1)*t*((s+1)*t + s) + 1) + b;},
		'easeInOutBack': function (t, b, c, d) {let s = 1.70158; t /= d/2; if (t < 1) {return c/2*(t*t*(((s*=(1.525))+1)*t - s)) + b;} return c/2*((t-=2)*t*(((s*=(1.525))+1)*t + s) + 2) + b;},
		'easeOutBounce': function (t, b, c, d) {return(t/=d)<1/2.75?7.5625*c*t*t+b:t<2/2.75?c*(7.5625*(t-=1.5/2.75)*t+0.75)+b:t<2.5/2.75?c*(7.5625*(t-=2.25/2.75)*t+0.9375)+b:c*(7.5625*(t-=2.625/2.75)*t+0.984375)+b;},
		'easeInBounce': function(t, b, c, d) {return c - ANIMATION_EASING_FUNCTIONS.easeOutBounce(d-t, 0, c, d) + b;},
		'easeInOutBounce': function (t, b, c, d) {if (t < d/2) {return ANIMATION_EASING_FUNCTIONS.easeInBounce (t*2, 0, c, d) * 0.5 + b;} return ANIMATION_EASING_FUNCTIONS.easeOutBounce (t*2-d, 0, c, d) * 0.5 + c*0.5 + b;}
	};
	/* eslint-enable */
	const ANIMATION_DEFAULT_OPTIONS = {
		'scrollLeft': 0,
		'scrollTop': 0,
		'duration': 200,
		'padTop': 0,
		'padLeft': 0,
		'easing': 'easeInOutQuad'
	};
	const FORM_OUTPUT_OPTIONS = {
		'duration': 10000,
		'scroll': true,
		'addClass': ['is-shown'],
		'removeClass': [],
		'escape': true
	};
	const CSV_OPTIONS = {
		'delimiter': ',',
		'lineEnding': '\r\n',
		'quote': '"',
		'trim': true,
		'parseHeader': true
	};
	const NOT_SET_OPTION_TEXT = ' -- (Not Set) --';
	const OUTPUT_TIMER_DATA_ATTRIBUTE = 'data-timer-id-by-domo';
	const DISBALED_FORM_DATA_ATTRIBUTE = 'data-disabled-by-domo';
	const DISABLED_FORM_ORIG_READONLY_DATA_ATTRIBUTE = 'data-orig-readonly-by-domo';
	const DISABLED_FORM_ORIG_DISABLED_DATA_ATTRIBUTE = 'data-orig-disabled-by-domo';

	// State
	let checkbox_radio_counter = 0;  // used to count how many checkboxes/radios we have made so far
	let listeners = []; // used for on() and off(). Each listener is an object literal {element, event, callback, label}

	/**
	 * @private
	 * Checks if a DOM elem has all data key-value pairs described
	 * @param {Object} data - object literal of attributes to check
	 *        {HTMLElement} dom_elem
	 * @return {boolean}
	 */
	function hasAllData(data, dom_elem) {
		for (let key in data) {
			if (!data.hasOwnProperty(key)) {
				continue;
			}

			if (dom_elem.getAttribute('data-' + key) !== data[key]) {
				return false;
			}
		}

		return true;
	} // hasAllData()

	/**
	 * Checks if an array contains all elements from another.
	 *
	 * @param {array} superset
	 * @param {array} subset
	 *
	 * @returns {boolean}
	 */
	function arrayContainsArray(superset, subset) {
		if (subset.length === 0) {
			return false;
		}
		return subset.every(function(value) {
			return superset.indexOf(value) >= 0;
		});
	} // arrayContainsArray()

	/**
	 * @private
	 * Equivalent of PHP's htmlspecialchars(), e.g.
	 * '&' (ampersand) becomes '&amp;'
	 * '<' (less than) becomes '&lt;'
	 * @param {string} str
	 * @return {string}
	 */
	function escapeHTML(str) {
		// Copied from eve-2.1.0 library. No need checks as we do them in the calling functions.
		return str
		.replace(/&/g, '&amp;')
		.replace(/</g, '&lt;')
		.replace(/>/g, '&gt;')
		.replace(/"/g, '&quot;')
		.replace(/'/g, '&#x27;')
		.replace(/\//g, '&#x2F;');
	} // escapeHTML()

	/**
	 * @private
	 * Inverse function of escapeHTML()
	 * @param {string} str
	 * @return {string}
	 */
	function unescapeHTML(str) {
		// Copied from eve-2.1.0 library. No need checks as we do them in the calling functions.
		return str
		.replace(/&amp;/g, '&')
		.replace(/&lt;/g, '<')
		.replace(/&gt;/g, '>')
		.replace(/&quot;/g, '"')
		.replace(/&#x27;/g, '\'')
		.replace(/&#039;/g, '\'')
		.replace(/&#x2F;/g, '/')
		.replace(/&#047;/g, '/')
		.replace(/\u00a0/g, ' ');
	} // unescapeHTML()

	/**
	 * @private
	 * Common function called by CSV-related functions
	 * @param {Object=} opts
	 * @return {Object}
	 */
	function processCSVOptions(opts) {
		let options = Object.assign({}, CSV_OPTIONS, opts);
		if (typeof options['delimiter'] !== 'string' || options['delimiter'].length !== 1) {
			options['delimiter'] = ',';
		}
		if (typeof options['lineEnding'] !== 'string' || options['lineEnding'].length > 2 || options['lineEnding'].length < 1) { // eslint-disable-line no-magic-numbers
			options['lineEnding'] = '\r\n';
		}
		if (typeof options['quote'] !== 'string' || options['quote'].length !== 1) {
			options['quote'] = '"';
		}
		if (typeof options['trim'] !== 'boolean') {
			options['trim'] = true;
		}
		if (typeof options['parseHeader'] !== 'boolean') {
			options['parseHeader'] = true;
		}

		return options;
	} // processCSVOptions()

	/**
	 * @private
	 * Common function called by CSV-related functions
	 * Turns text into safely quoted CSV cell value, if necessary
	 * @param {string} value
	 *        {Object} options
	 * @return {string}
	 */
	function processCSVCellValue(value, opts) {
		// values containing line endings, quotes and commas must be enclosed
		if (value.indexOf(opts['delimiter']) !== -1 || value.indexOf(opts['quote']) !== -1 || value.indexOf(opts['lineEnding']) !== -1) {
			let regex = new RegExp(opts['quote'], 'g');
			return opts['quote'] + value.replace(regex, opts['quote'] + opts['quote']) + opts['quote']; // escape quotes
		}

		return value;
	} // processCSVCellValue()

	/**
	 * @private
	 * Common function called by animation functions
	 * @param {Object} options
	 * @return {Object} options
	 */
	function processAnimationOptions(opts) {
		let options = Object.assign({}, ANIMATION_DEFAULT_OPTIONS, opts); // Clone options

		if (typeof options['duration'] !== 'number') {
			options['duration'] = ANIMATION_DEFAULT_OPTIONS['duration'];
		}
		if (options['duration'] === 0) {
			options['duration'] = 1; // prevent any potential divide-by-zero errors
		}

		if (typeof options['easing'] === 'string') {
			// Ensure user-specified easing exists, else use default.
			if (options['easing'] in ANIMATION_EASING_FUNCTIONS) {
				options['easing'] = ANIMATION_EASING_FUNCTIONS[options['easing']];
			}
			else {
				console.warn('Unknown eaasing: ' + options['easing'] + '. Default to easing: ' + ANIMATION_DEFAULT_OPTIONS['easing']);
				options['easing'] = ANIMATION_EASING_FUNCTIONS[ANIMATION_DEFAULT_OPTIONS['easing']];
			}
		}
		else if (typeof options['easing'] === 'function') {
			options['easing'] = ANIMATION_EASING_FUNCTIONS[ANIMATION_DEFAULT_OPTIONS['easing']];
		}
		else {
			console.warn('Unknown eaasing type: ' + typeof options['easing'] + '. Default to easing: ' + ANIMATION_DEFAULT_OPTIONS['easing']);
			options['easing'] = ANIMATION_EASING_FUNCTIONS[ANIMATION_DEFAULT_OPTIONS['easing']];
		}

		if (typeof options['padTop'] !== 'number') {
			options['padTop'] = ANIMATION_DEFAULT_OPTIONS['padTop'];
		}
		if (typeof options['padLeft'] !== 'number') {
			options['padLeft'] = ANIMATION_DEFAULT_OPTIONS['padLeft'];
		}

		if (typeof options['scrollTop'] !== 'number') {
			options['scrollTop'] = ANIMATION_DEFAULT_OPTIONS['scrollTop'];
		}
		if (typeof options['scrollLeft'] !== 'number') {
			options['scrollLeft'] = ANIMATION_DEFAULT_OPTIONS['scrollLeft'];
		}

		return options;
	} // processAnimationOptions()

	/**
	 * @private
	 * Common function called by form ouput functions
	 * @param {Object} options
	 * @return {Object} options
	 */
	function processFormOutputOptions(opts) {
		let options = Object.assign({}, FORM_OUTPUT_OPTIONS, opts); // Clone options

		if (typeof options['duration'] !== 'number') {
			options['duration'] = FORM_OUTPUT_OPTIONS['duration'];
		}
		if (typeof options['scroll'] !== 'boolean') {
			options['scroll'] = FORM_OUTPUT_OPTIONS['scroll'];
		}
		if (typeof options['addClass'] === 'string') {
			options['addClass'] = [options['addClass']]; // put it in array to standardize operations later
		}
		else if (!(options['addClass'] instanceof Array)) {
			options['addClass'] = FORM_OUTPUT_OPTIONS['addClass'];
		}
		if (typeof options['removeClass'] === 'string') {
			options['removeClass'] = [options['removeClass']]; // put it in array to standardize operations later
		}
		else if (!(options['removeClass'] instanceof Array)) {
			options['removeClass'] = FORM_OUTPUT_OPTIONS['removeClass'];
		}
		if (typeof options['escape'] !== 'boolean') {
			options['escape'] = FORM_OUTPUT_OPTIONS['escape'];
		}

		return options;
	} // processFormOutputOptions()

/*******************
*  Document-level  *
********************/
	/**
	 * Returns the content of a meta tag by its name
	 * - searches OpenGraph metatags by "property" and all other metatags by "name"
	 * @param {string} n
	 * @return {string | null}
	 */
	function getMeta(n) {
		if (typeof n !== 'string') {
			throw new TypeError('getMeta() - expects string argument, got: ' + typeof n);
		}

		let metas = document.getElementsByTagName('meta');

		// Look through all existing meta tags
		if (n.indexOf('og:') === 0) {
			for (let i = 0, len = metas.length; i < len; i++) {
				if (n === metas[i].getAttribute('property')) {
					return metas[i].content;
				}
			}
		} // Open Graph tag
		else {
			for (let i = 0, len = metas.length; i < len; i++) {
				if (n === metas[i].name) {
					return metas[i].content;
				}
			}
		} // Normal meta
		return null;
	} // getMeta()

	/**
	 * Sets a meta in <head>. If meta does not exist, set it.
	 * - sets Open Graph metatags by "property" and all other metatags by "name"
	 * @param {string} name
	 *        {string} content
	 * @return {HTMLMetaElement}
	 */
	function setMeta(n, content) {
		if (typeof n !== 'string') {
			throw new TypeError('setMeta() - expects string argument for name, got: ' + typeof n);
		}
		if (typeof content !== 'string') {
			throw new TypeError('setMeta() - expects string argument for content, got: ' + typeof content);
		}

		let metas = document.getElementsByTagName('meta');

		// Look through all existing meta tags
		for (let i = 0, len = metas.length; i < len; i++) {
			if (n === metas[i].name) {
				metas[i].content = content;
				return metas[i];
			} // Normal meta
			if (n === metas[i].getAttribute('property')) {
				metas[i].content = content;
				return metas[i];
			} // Open Graph tag
		}

		// Add a new meta
		let tag = document.createElement('meta');
		if (n.indexOf('og:') === 0) {
			tag.setAttribute('property', n);
		} // Open Graph tag
		else {
			tag.name = n;
		}
		tag.content = content;
		document.getElementsByTagName('head')[0].appendChild(tag);
		return tag;
	} // setMeta()

	/**
	 * @param {string} file_path
	 * @param {Object=} data_attributes (optional) - key-value pairs of HTML5 data attributes
	 * @return {Promise} resolves / rejects with HTMLLinkElement
	 */
	function addCSSFile(file_path, data_attributes) {
		if (typeof file_path !== 'string') {
			throw new TypeError('addCSSFile() - expects string argument for file path, got: ' + typeof file_path);
		}

		// Clone to avoid mutating param
		let data = Object.assign({}, data_attributes);

		// Create <link> with href and data-* attributes
		let link = document.createElement('link');
		link.setAttribute('rel', 'stylesheet');
		link.setAttribute('href', file_path);
		for (let prop in data) {
			if (!data.hasOwnProperty(prop)) {
				continue;
			}

			link.setAttribute('data-' + prop, data[prop]);
		}
		document.getElementsByTagName('head')[0].appendChild(link);

		return new Promise(function(resolve, reject) {
			link.onload = function() {
				cleanUp();
				resolve(link);
			};

			link.onerror = function() {
				cleanUp();
				reject(new Error('Error loading link: ' + file_path));
			};

			link.onabort = function() {
				cleanUp();
				reject(new Error('Aborted'));
			};
		});

		function cleanUp() {
			link.onload = null;
			link.onerror = null;
			link.onabort = null;
		}

		// return link;
	} // addCSSFile()

	/**
	 * @param {string} file_path
	 * @return {number} of link tags removed
	 */
	function removeCSSFile(file_path) {
		let links = [].slice.call(document.getElementsByTagName('link'));
		let removed_count = 0;
		for (let i = 0, len = links.length; i < len; i++) {
			let link = links[i];
			if (link.getAttribute('href') === file_path) {
				link.setAttribute('disabled', 'disabled');
				link.remove();
				removed_count++;
			}
		}

		return removed_count;
	} // removeCSSFile()

	/**
	 * Note: only files that match all attributes specified will be removed
	 * @param {Object} data attributes to check against
	 * @return {number} of link tags removed
	 */
	function removeCSSFileByData(data_attributes) {
		if (typeof data_attributes !== 'object') {
			return 0;
		}

		let links = [].slice.call(document.getElementsByTagName('link'));
		let removed_count = 0;
		for (let i = 0; i < links.length; i++) {
			let link = links[i];
			if (hasAllData(data_attributes, link)) {
				link.setAttribute('disabled', 'disabled');
				link.remove();
				removed_count++;
			}
		}

		// console.log('removing ' + removed_count + ' CSS');
		return removed_count;
	} // removeCSSFileByData()

	/**
	 * @param {string} file_path
	 *         {Object=} data_attributes (optional) - key-value pairs of HTML5 data attributes
	 * @return {Promise} resolves / rejects with HTMLScriptElement
	 */
	function addJSFile(file_path, data_attributes) {
		if (typeof file_path !== 'string') {
			throw new TypeError('addJSFile() - expects string argument for file path, got: ' + typeof file_path);
		}

		// Clone to avoid mutating param
		let data = Object.assign({}, data_attributes);

		// Create <script> with src and data-* attributes
		let script = document.createElement('script');
		script.type = 'text/javascript';
		script.src = file_path;
		for (let prop in data) {
			if (!data.hasOwnProperty(prop)) {
				continue;
			}

			script.setAttribute('data-' + prop, data[prop]);
		}

		document.body.appendChild(script);

		return new Promise(function(resolve, reject) {
			script.onload = function() {
				cleanUp();
				resolve(script);
			};

			script.onerror = function() {
				cleanUp();
				reject(new Error('Error loading script: ' + file_path));
			};

			script.onabort = function() {
				cleanUp();
				reject(new Error('Aborted'));
			};
		});
		function cleanUp() {
			script.onload = null;
			script.onerror = null;
			script.onabort = null;
		}

		// return script;
	} // addJSFile()

	/**
	 * @param {string} file_path
	 * @return {number} of script tags removed
	 */
	function removeJSFile(file_path) {
		let scripts = [].slice.call(document.getElementsByTagName('script'));
		let removed_count = 0;
		for (let i = 0, len = scripts.length; i < len; i++) {
			let script = scripts[i];
			if (script.getAttribute('src') === file_path) {
				script.remove();
				removed_count++;
			}
		}

		return removed_count;
	} // removeJSFile()

	/**
	 * Note: only files that match all attributes specified will be removed
	 * @param {Object} data attributes to check against
	 * @return {number} of script tags removed
	 */
	function removeJSFileByData(data_attributes) {
		if (typeof data_attributes !== 'object') {
			return 0;
		}

		let scripts = [].slice.call(document.getElementsByTagName('script'));
		let removed_count = 0;
		for (let i = 0, len = scripts.length; i < len; i++) {
			let script = scripts[i];
			if (script.src !== '' && hasAllData(data_attributes, script)) { // only remove scripts referring to files (has src)
				script.remove();
				removed_count++;
			}
		}

		// console.log('removing ' + removed_count + ' JS');
		return removed_count;
	} // removeJSFileByData()

	/**
	 * Converts a string containing HTML into DOM Elements
	 * @param {string} s
	 * @return {Array} of HTMLElement
	 */
	function str2dom(s) {
		let div = document.createElement('div');
		div.innerHTML = s;
		return [].slice.call(div.children); // because we like stable, non-live lists
	} // str2dom()

	/**
	 * Gets the text currently selected (highlighted) in the document
	 * @return {string}
	 */
	function getSelectedText() {
		if (window.getSelection) { // Other browsers
			return window.getSelection().toString();
		}
		if (document.selection) {
			return document.selection.createRange().htmlText;
		}

		throw new Error('getSelectedText() - this browser does not support selecting text.');
	} // getSelectedText()

/******************
*  Element-level  *
*******************/
	/**
	 * Selects all text in an element (akin to highlighting with your mouse)
	 * @param {HTMLElement} elem
	 */
	function selectText(elem) {
		if (typeof elem !== 'object' || !(elem instanceof HTMLElement)) {
			throw new TypeError('selectText() - param is not an HTMLElement. (' + typeof elem + ')');
		}

		let selection = window.getSelection();
		let range = document.createRange();
		range.selectNodeContents(elem);
		selection.removeAllRanges();
		selection.addRange(range);
	} // selectText()

	/**
	 * De-selects all text
	 */
	function clearSelection() {
		window.getSelection().removeAllRanges();
	} // clearSelection()

	/**
	 * @param {Node} node - to be wrapped
	 *         {string=} tag_name (optional) - of wrapper to be created (DEFAULT: div)
	 * @return {HTMLElement} wrapper
	 */
	function wrapNode(node, tag_name) {
		if (typeof node !== 'object' || !(node instanceof Node)) {
			throw new Error('wrapNode() - node is not an instance of Node');
		}
		let wrapper;
		if (typeof tag_name === 'string') {
			wrapper = document.createElement(tag_name);
		}
		else {
			wrapper = document.createElement('div');
		}
		node.parentNode.insertBefore(wrapper, node.nextSibling);
	    wrapper.appendChild(node);

	    return wrapper;
	} // wrapNode

	/**
	 * Locks scroll position of an element, and removes scrollbars
	 * - Note: if already locked, operation will fail
	 * @param {HTMLElement} elem
	 * @return {boolean} success
	 */
	function lockScroll(dom_elem) {
		// Always work on body instead of html
		let dom_curr = dom_elem;
		if (dom_curr === document.scrollingElement) {
			dom_curr = document.body;
		}

		let state = dom_curr.getAttribute(LOCK_SCROLL_STATE_DATA_ATTRIBUTE);
		if (state === 'locked') {
			// console.log('Already locked');
			return false;
		}

		dom_curr.setAttribute(LOCK_SCROLL_STATE_DATA_ATTRIBUTE, 'locked');

		// Remember scroll position
		dom_curr.setAttribute(LOCK_SCROLL_SCROLL_LEFT_DATA_ATTRIBUTE, dom_curr.scrollLeft);
		dom_curr.setAttribute(LOCK_SCROLL_SCROLL_TOP_DATA_ATTRIBUTE, dom_curr.scrollTop);

		// Remember overflow setting
		let cs = window.getComputedStyle(dom_curr, null);
		dom_curr.setAttribute(LOCK_SCROLL_OVERFLOW_X_DATA_ATTRIBUTE, cs.overflowX);
		dom_curr.setAttribute(LOCK_SCROLL_OVERFLOW_Y_DATA_ATTRIBUTE, cs.overflowY);
		dom_curr.style.overflowX = 'hidden';
		dom_curr.style.overflowY = 'hidden';

		return true;
	} // lockScroll()

	/**
	 * Unlocks an element that was locked by lockScroll()
	 * @param {HTMLElement} elem
	 * @return {boolean} success
	 */
	function unlockScroll(dom_elem) {
		// Always work on body instead of html
		let dom_curr = dom_elem;
		if (dom_curr === document.scrollingElement) {
			dom_curr = document.body;
		}

		let state = dom_curr.getAttribute(LOCK_SCROLL_STATE_DATA_ATTRIBUTE);
		if (state !== 'locked') {
			// console.log('Not locked');
			return false;
		}

		// Recall overflow settings
		dom_curr.style.overflowX = dom_curr.getAttribute(LOCK_SCROLL_OVERFLOW_X_DATA_ATTRIBUTE);
		dom_curr.style.overflowY = dom_curr.getAttribute(LOCK_SCROLL_OVERFLOW_Y_DATA_ATTRIBUTE);
		dom_curr.removeAttribute(LOCK_SCROLL_OVERFLOW_X_DATA_ATTRIBUTE);
		dom_curr.removeAttribute(LOCK_SCROLL_OVERFLOW_Y_DATA_ATTRIBUTE);

		// Recall scroll position
		dom_curr.scrollLeft = parseFloat(dom_curr.getAttribute(LOCK_SCROLL_SCROLL_LEFT_DATA_ATTRIBUTE));
		dom_curr.scrollTop = parseFloat(dom_curr.getAttribute(LOCK_SCROLL_SCROLL_TOP_DATA_ATTRIBUTE));
		dom_curr.removeAttribute(LOCK_SCROLL_SCROLL_LEFT_DATA_ATTRIBUTE);
		dom_curr.removeAttribute(LOCK_SCROLL_SCROLL_TOP_DATA_ATTRIBUTE);

		dom_curr.removeAttribute(LOCK_SCROLL_STATE_DATA_ATTRIBUTE);

		return true;
	} // unlockScroll

	/**
	 * Checks if element's scroll has been locked by lockScroll()
	 * @param {HTMLElement} elem
	 * @return {boolean}
	 */
	function isLockedScroll(dom_elem) {
		// Always work on body instead of html
		let dom_curr = dom_elem;
		if (dom_curr === document.scrollingElement) {
			dom_curr = document.body;
		}

		let state = dom_curr.getAttribute(LOCK_SCROLL_STATE_DATA_ATTRIBUTE);
		return state === 'locked';
	} // isLockedScroll()

	/**
	 * Gets position of element w.r.t. screen, i.e. does not include scrollTop & scrollLeft amounts.
	 * @param {HTMLElement} elem
	 * @return {Object} point coordinates, e.g. {x:1, y:2}
	 */
	function getPositionOnScreen(dom_elem) {
		if (typeof dom_elem !== 'object' || !(dom_elem instanceof HTMLElement)) {
			throw new TypeError('getPositionOnScreen() - param is not an HTMLElement. (' + typeof dom_elem + ')');
		}

		let elem_rect = dom_elem.getBoundingClientRect();

		return {
			'x': elem_rect.left - document.documentElement.clientLeft,
			'y': elem_rect.top - document.documentElement.clientTop
		};
	} // getPositionOnScreen()

	/**
	 * Gets position of element w.r.t. page, i.e. position off the top left corner of the document
	 * @param {HTMLElement} dom_elem
	 * @return {Object} point coordinates, e.g. {x:1, y:2}
	 */
	function getPositionOnPage(dom_elem) {
		if (typeof dom_elem !== 'object' || !(dom_elem instanceof HTMLElement)) {
			throw new TypeError('getPositionOnPage() - param is not an HTMLElement. (' + typeof dom_elem + ')');
		}

		let x = 0;
		let y = 0;
		let dom_curr = dom_elem;
		while (dom_curr) {
			y += dom_curr.offsetTop || 0;
			x += dom_curr.offsetLeft || 0;
			dom_curr = dom_curr.offsetParent;
		}

		return {
			'x': x,
			'y': y
		};
	} // getPositionOnPage()

	/**
	 * Checks if the middle half of an element is within viewport
	 * @param {HTMLElement} dom_elem
	 * @return {boolean}
	 */
	function isMiddleInViewport(dom_elem) {
		// Elem pos
		let pos = getPositionOnPage(dom_elem);
		let top_quart = pos['y'] + (dom_elem.offsetHeight / 4);        // eslint-disable-line no-magic-numbers
		let bottom_quart = pos['y'] + (dom_elem.offsetHeight * 3 / 4); // eslint-disable-line no-magic-numbers

		// Window state
		let window_scroll_top = document.scrollingElement.scrollTop;
		let window_height = document.scrollingElement.clientHeight;

		return top_quart > window_scroll_top && bottom_quart < window_scroll_top + window_height;
	} // isMiddleInViewport()

	/**
	 * Checks if any part of an element is within viewport
	 * @param {HTMLElement} dom_elem
	 * @return {boolean}
	 */
	function isPartiallyInViewport(dom_elem) {
		let rect = dom_elem.getBoundingClientRect();

		let window_height = document.scrollingElement.clientHeight;
		let window_width = document.scrollingElement.clientWidth;

		return Boolean(rect) &&
			rect['bottom'] >= 0 &&
			rect['right'] >= 0 &&
			rect['top'] <= window_height &&
			rect['left'] <= window_width;
	} // isPartiallyInViewport()

	/**
	 * @param {HTMLElement} dom_elem
	 *         {Object=} opts (optional)
     *            {number} duration    (ms)
     *            {string} easing      name of easing function to use, e.g. "linear", "easeInOutQuad"
     *            {number} scrollLeft  (px)
     *            {number} scrollTop   (px)
     * @return {Promise} resolves when completed, rejects on error (should never happen).
	 */
	function animateScroll(dom_elem, opts) {
		// Process options
		let options = processAnimationOptions(opts);

		// State
		let start_time; // to start on first frame
		let start_values = {
			'scrollLeft': dom_elem.scrollLeft,
			'scrollTop': dom_elem.scrollTop
		};
		let change_values = {
			'scrollLeft': options['scrollLeft'] - start_values['scrollLeft'],
			'scrollTop': options['scrollTop'] - start_values['scrollTop']
		};

		return new Promise(function(resolve, reject) {
			function step(timestamp) {
				if (!start_time) {
					start_time = timestamp;
				}

				let elapsed_duration = timestamp - start_time;

				// Render the new position
				if (elapsed_duration < options['duration']) {
					dom_elem.scrollLeft = options['easing'](elapsed_duration, start_values['scrollLeft'], change_values['scrollLeft'], options['duration']); // t, b, c, d;
					dom_elem.scrollTop = options['easing'](elapsed_duration, start_values['scrollTop'], change_values['scrollTop'], options['duration']); // t, b, c, d;

					window.requestAnimationFrame(step);
				}
				else {
					dom_elem.scrollLeft = options['scrollLeft'];
					dom_elem.scrollTop = options['scrollTop'];

					resolve();
				} // Last Frame
			} // step()

			try {
				window.requestAnimationFrame(step); // note: requestAnimationFrame calls with a timestamp (ms) as param
			}
			catch (e) {
				console.error(e);
				reject(e);
			}
		});
	} // animateScroll()

	/**
	 * Simulates element.scrollIntoView() with scrollIntoViewOptions of {behaviour: 'smooth'}
	 * - Allows padTop and padLeft, which native scrollIntoView() does not
	 * - Note: uses box-sizing of border-box
	 * @param {HTMLElement} dom_orig_elem
	 *         {Object=} options (optional)
	 *            {number} duration    (ms) DEFAULT: 200
     *            {string} easing      name of easing function to use, e.g. "linear". DEFAULT: "easeInOutQuad"
     *            {string} block       One of "start", "center", "end", or "nearest". DEFAULT: "start"
     *            {string} inline      One of "start", "center", "end", or "nearest". DEFAULT: "nearest"
     *            {number} padTop      (px)
     *            {number} padLeft     (px)
     * @return {Promise} resolves when completed, rejects on error (should never happen).
	 */
	function animateScrollIntoView(dom_elem, opts) {
		// Process options
		let options = processAnimationOptions(opts);

		let dom_parent_client_height = document.scrollingElement.clientHeight;
		let dom_parent_client_width = document.scrollingElement.clientWidth;
		let dom_parent_scroll_height = document.scrollingElement.scrollHeight;
		let dom_parent_scroll_width = document.scrollingElement.scrollWidth;

		// Find target vertical pos
		let pos = getPositionOnPage(dom_elem);
		if (options['block'] === 'end') {
			options['scrollTop'] = pos['y'] - options['padTop'] + dom_elem.offsetHeight - document.scrollingElement.clientHeight;
		}
		else if (options['block'] === 'center') {
			options['scrollTop'] = pos['y'] - options['padTop'] - ((document.scrollingElement.clientHeight - dom_elem.offsetHeight) / 2);  // eslint-disable-line no-magic-numbers
		}
		else if (options['block'] === 'nearest') {
			if (pos['y'] > document.scrollingElement.scrollTop) {
				options['scrollTop'] = pos['y'] - options['padTop'];
			}
			else if (pos['y'] + dom_elem.offsetHeight < document.scrollingElement.scrollTop) {
				options['scrollTop'] = pos['y'] - options['padTop'] + dom_elem.offsetHeight - document.scrollingElement.clientHeight;
			}
			else {
				options['scrollTop'] = document.scrollingElement.scrollTop;
			}
		}
		else {
			options['scrollTop'] = pos['y'] - options['padTop'];
		}

		// Find target horizontal pos
		if (options['inline'] === 'end') {
			options['scrollLeft'] = pos['x'] - options['padLeft'] + dom_elem.offsetWidth - document.scrollingElement.clientWidth;
		}
		else if (options['inline'] === 'center') {
			options['scrollLeft'] = pos['x'] - options['padLeft'] - ((document.scrollingElement.clientWidth - dom_elem.offsetWidth) / 2);  // eslint-disable-line no-magic-numbers
		}
		else if (options['inline'] === 'start') {
			options['scrollLeft'] = pos['x'] - options['padLeft'];
		}
		else {
			if (pos['x'] > document.scrollingElement.scrollLeft) {
				options['scrollLeft'] = pos['x'] - options['padLeft'];
			}
			else if (pos['x'] + dom_elem.offsetWidth < document.scrollingElement.scrollLeft) {
				options['scrollLeft'] = pos['x'] - options['padLeft'] + dom_elem.offsetWidth - document.scrollingElement.clientWidth;
			}
			else {
				options['scrollLeft'] = document.scrollingElement.scrollLeft;
			}
		}
		
		// Scroll to element
		return animateScroll(document.scrollingElement, options);
	} // animateScrollIntoView()

	/**
	 * Finds one element (relative to component element).
	 * @param {string} selector
	 *        {HTMLElement} dom_from
	 * @return {HTMLElement | null } element, or null if no matches found.
	 */
	function findOne(selector, dom_from) {
		let dom_elem = dom_from;
		if (typeof dom_elem === 'undefined') {
			dom_elem = document.documentElement;
		}
		try {
			let x = dom_elem.querySelector(selector);
			// if (x === null) { console.warn('findOne(): [' + component + '] ' + selector + ' - not found.'); }
			return x;
		}
		catch (error) {
			throw new Error(error.message + ' (' + selector + ')');
		}
	} // findOne()

	/**
	 * Finds elements (relative to component element).
	 * - Note: returns Array rather than NodeList / HTMLCollection
	 * @param {string} selector
	 *        {HTMLElement} dom_from
	 * @return {Array} list of HTMLElements, empty list if no matches found.
	 */
	function findAll(selector, dom_from) {
		let dom_elem = dom_from;
		if (typeof dom_elem === 'undefined') {
			dom_elem = document.documentElement;
		}
		try {
			let nodelist = dom_elem.querySelectorAll(selector);
			let array = Array.from(nodelist);
			// if (array.length === 0) { console.warn('findAll(): [' + component + '] ' + selector + ' - not found.'); }
			return array;
		}
		catch (error) {
			throw new Error(error.message + ' (' + selector + ')');
		}
	} // findAll()

	/**
	 * Adds an event listener. Managing event listeners here makes it easy to bulk add and remove them.
	 * - Note: listen on the bubbling phase, i.e. useCapture: false
	 * @param {HTMLElement | Array} element - or array of elements
	 *        {string} events - space separated e.g. "click", "keydown", "submit"
	 *        {function} callback
	 *        {stirng} label - e.g. "page"
	 */
	function on(element, events, callback, label) {
		if (typeof events !== 'string') {
			throw new TypeError('on() - events is not string. ' + typeof events);
		}
		if (typeof callback !== 'function') {
			throw new TypeError('on() - callback is not function. ' + typeof callback);
		}
		if (typeof label !== 'string') {
			throw new TypeError('on() - label is not string. ' + typeof label);
		}

		// Handle Arrays
		if (element instanceof Array) {
			for (let i = 0, len = element.length; i < len; i++) {
				on(element[i], events, callback, label);
			}
			return;
		}

		// Process element param
		if (typeof element === 'undefined' || element === null) {
			throw new ReferenceError('on() - element is ' + element + ' . (Event: ' + events + ')');
		}
		if (!element.addEventListener) {
			throw new TypeError('on() - element is not HTMLElement. ' + typeof element);
		}

		// Bind callbacks
		let events_array = events.split(' ').filter(function(el) {
			return el !== '';
		});
		for (let i = 0, len = events_array.length; i < len; i++) {
			element.addEventListener(events_array[i], callback);

			listeners.push({
				'element': element,
				'event': events_array[i],
				'callback': callback,
				'label': label
			});
		} // for each event
	} // on()

	/**
	 * Removes an event listener. Managing event listeners here makes it easy to bulk add and remove them.
	 * - if the same callback function was binded multiple times through on(), they will all be removed.
	 * - params are all optional:
	 *      - 1 param: off(label)           listeners for all events of all elements of this label will be removed.
	 *      - 1 param: off(element)         all listeners for all events of this element will be removed.
	 *      - 2 param: off(element, events) all listeners for these events of this element(s) will be removed.
	 *      - 3 param: off(element, events, callback) the listener for this event of this element(s) will be removed.
	 *      - 4 param: off(element, events, callback, label) all listeners for this event of this element(s) will be removed.
	 */
	function off() {
		let element, events, callback, label; // for params

		// Figure out params
		if (arguments.length === 0) {
			throw new Error('off() - expects at least 1 argument.');
		}
		else if (arguments.length === 1) {
			if (typeof arguments[0] === 'string') {
				label = arguments[0];

				// Remove all listeners with this label
				for (let i = listeners.length - 1; i >= 0; i--) {
					let listener = listeners[i];

					if (listener['label'] === label) {
						listeners.splice(i, 1);
						listener['element'].removeEventListener(listener['event'], listener['callback']);
					}
				} // loop backwards

				return;
			} // label

			element = arguments[0];
		} // 1 param
		else if (arguments.length === 2) {  // eslint-disable-line no-magic-numbers
			element = arguments[0];
			events = arguments[1];
		} // 2 params
		else if (arguments.length === 3) {  // eslint-disable-line no-magic-numbers
			element = arguments[0];
			events = arguments[1];
			callback = arguments[2];
		}
		else if (arguments.length === 4) {  // eslint-disable-line no-magic-numbers
			element = arguments[0];
			events = arguments[1];
			callback = arguments[2];
			label = arguments[3];
		}

		// Check label
		if (typeof label !== 'undefined' && typeof label !== 'string') {
			throw new TypeError('off() - label is not string. ' + typeof label);
		}

		// Check callback
		if (typeof callback !== 'undefined' && typeof callback !== 'function') {
			throw new TypeError('off() - callback is not function. ' + typeof callback);
		}

		// Check events
		if (typeof events !== 'undefined' && typeof events !== 'string') {
			throw new TypeError('off() - events is not string. ' + typeof events);
		}
		let events_array;
		if (events) {
			events_array = events.split(' ').filter(function(el) {
				return el !== '';
			});

			if (events_array.length === 0) {
				throw new RangeError('off() - no events specified.');
			}
		}

		// Check element
		if (element instanceof Array) {
			// Iterate all elements
			for (let i = 0, len = element.length; i < len; i++) {
				if (!events) {
					off(element[i]);
				}
				else if (!callback) {
					off(element[i], events);
				}
				else if (!label) { // eslint-disable-line no-negated-condition
					off(element[i], events, callback);
				}
				else {
					off(element[i], events, callback, label);
				}
			}
		} // array of elements
		else if (element.addEventListener) {
			for (let i = listeners.length - 1; i >= 0; i--) {
				let listener = listeners[i];

				if (listener['element'] !== element) {
					continue;
				}

				if (events && !events_array.includes(listener['event'])) {
					continue;
				}

				if (callback && listener['callback'] !== callback) {
					continue;
				}

				if (label && listener['label'] !== label) {
					continue;
				}

				listeners.splice(i, 1);
				listener['element'].removeEventListener(listener['event'], listener['callback']);
			} // loop backwards
		} // single element
		else {
			throw new TypeError('off() - element is not HTMLElement. ' + typeof element);
		}
	} // off()

	/**
	 * Gets the number of listeners currently attached to an element, or to all elements
	 * @param {HTMLElement=} element
	 * @return {number}
	 */
	function getListenerCount(element) {
		if (typeof element === 'undefined') {
			return listeners.length;
		}

		return listeners.filter(function(el) {
			return el['element'] === element;
		}).length;
	} // getListenerCount()

/*****************
*  Form related  *
******************/
	/**
	 * Populates a form with data
	 * - Note: not for file inputs
	 * - Note: elements with no "name" attribute will not be affected.
	 * - Note: boolean values in form_data will be casted to strings "1" and "0"
	 * @param {HTMLFormElement} dom_form
	 *        {Object} form_data - each key corresponds to a form field "name"
	 */
	function populateFormValues(dom_form, form_data) {
		// Check params
		if (!(dom_form instanceof HTMLFormElement)) {
			throw new TypeError('populateFormValues() - param is not an HTMLFormElement.  (' + typeof dom_form + ')');
		}
		if (typeof form_data !== 'object') {
			throw new TypeError('populateFormValues() - form_data is not an object. (' + typeof form_data + ')');
		}

		for (let i = 0, len = dom_form.elements.length; i < len; i++) {
			let elem_name = dom_form.elements[i].name;
			if (!elem_name) {
				continue;
			}

			// Skip file inputs
			let form_elem_type = dom_form.elements[elem_name].type;
			if (form_elem_type === 'file') {
				continue;
			}

			// Handle checkboxes
			if (form_elem_type === 'checkbox') {
				if (form_data[elem_name]) {
					dom_form.elements[elem_name].checked = true;
				}
				else {
					dom_form.elements[elem_name].checked = false;
				}
				continue;
			}

			// Handle radios
			if (form_elem_type === 'radio') {
				if (dom_form.elements[elem_name].value === String(form_data[elem_name])) { // always compare as strings
					dom_form.elements[elem_name].checked = true;
				}
				else {
					dom_form.elements[elem_name].checked = false;
				}
				continue;
			}

			// Handle other inputs (presumably text or text-compatible)
			if (form_data[elem_name] === true) {
				dom_form.elements[elem_name].value = '1';
			}
			else if (form_data[elem_name] === false) {
				dom_form.elements[elem_name].value = '0';
			}
			else if (form_data[elem_name] === null || typeof form_data[elem_name] === 'undefined') {
				dom_form.elements[elem_name].value = '';
			}
			else {
				dom_form.elements[elem_name].value = form_data[elem_name];
			}
		} // for each form element
	} // populateFormValues()

	/**
	 * Populates a <select> with children <option> elements using an array of object literals.
	 * - Note: any existing options in the select will be wiped out
	 * @param {HTMLSelectElement} dom_select
	 * @param {Array} options -  objects in the format of {"value": "HTML Text"}
	 * @return {number} count of options added
	 */
	function populateSelect(dom_select, options) {
		// Check params
		if (!(dom_select instanceof HTMLSelectElement)) {
			throw new TypeError('populateSelect() - param is not an HTMLSelectElement.  (' + typeof dom_select + ')');
		}
		if (!(options instanceof Array)) {
			throw new TypeError('populateSelect() - options is not an Array. (' + typeof options + ')');
		}

		// Do work
		let count = 0;
		let html = '';
		let has_optgroup = false;
		for (let i = 0, len = options.length; i < len; i++) {
			if (typeof options[i] !== 'object') {
				continue;
			}

			// empty options denote new section
			if (Object.keys(options[i]).length === 0) {
				html += '</optgroup><optgroup>';
				has_optgroup = true;
				continue;
			}

			let value = Object.keys(options[i])[0];
			let text = options[i][value];
			html += '<option value="' + value + '">' + text + '</option>';
			count++;
		}
		if (has_optgroup) {
			html = '<optgroup>' + html + '</optgroup>';
		}

		dom_select.innerHTML = html;

		return count;
	} // populateSelect()

	/**
	 * Populate all <select> with <option> elements, with default values in the corresponding "data-options" and "data-default" attributes
	 * - Note: any existing options in the select will be wiped out
	 * - Note: <select> without "data-options" are not populated
	 * @param {HTMLElement} dom_from - where to find select boxes from
	 * @param {Object} options
	 * @parm {string} not_set_option_text - used if no default value is set.
	*/
	function populateSelects(dom_from, options, not_set_option_text) {
		let nsot = not_set_option_text;
		if (typeof nsot !== 'string') {
			nsot = NOT_SET_OPTION_TEXT;
		}

		// Populate selects
		let key, default_value, option_list;
		let dom_selects = findAll('select', dom_from);
		for (let i = 0, len = dom_selects.length; i < len; i++) {
			key = dom_selects[i].getAttribute('data-options');
			default_value = dom_selects[i].getAttribute('data-default');
			if (!key) {
				continue;
			}

			if (!options[key]) {
				console.warn('options[' + key + '] not found.');
				continue;
			}

			// Create option list and populate
			option_list = options[key].slice(0);
			if (!default_value) {
				option_list.unshift({'': nsot});
			}
			populateSelect(dom_selects[i], option_list);
			if (default_value) {
				dom_selects[i].value = default_value;
			}
		}
	} // populateSelects()

	/**
	 * Populates a <datalist> with children <option> elements using an array of strings
	 * - Note: any existing options in the datalist will be wiped out
	 * @param {HTMLDataListElement} dom_datalist
	 * @param {Array} options - strings
	 * @return {number} count of options added
	 */
	function populateDatalist(dom_datalist, options) {
		// Check params
		if (!(dom_datalist instanceof HTMLDataListElement)) {
			throw new TypeError('populateDatalist() - param is not an HTMLDataListElement.  (' + typeof dom_datalist + ')');
		}
		if (!(options instanceof Array)) {
			throw new TypeError('populateDatalist() - options is not an Array. (' + typeof options + ')');
		}

		// Do work
		let count = 0;
		let html = '';
		for (let i = 0, len = options.length; i < len; i++) {
			if (typeof options[i] !== 'string') {
				continue;
			}

			html += '<option value="' + options[i] + '">';
			count++;
		}
		dom_datalist.innerHTML = html;

		return count;
	} // populateDatalist()

	/**
	 * Creates checkbox children using an array of object literals.
	 * - Note: any existing HTML in the div will be wiped out
	 * @param {HTMLElement} dom_container - container for generated checkboxes
	 * @param {Array} options - objects in the format of {"value": "HTML Text"}
	 * @param {string} name_attribute - name attribute of the checkboxes. Optional. (DEFAULTS to "checkbox")
	 * @return {number} count of checkboxes added
	 */
	function populateCheckboxes(dom_container, options, name_attribute) {
		// Check params
		if (!(dom_container instanceof HTMLElement)) {
			throw new TypeError('populateCheckboxes() - param is not an HTMLElement.  (' + typeof dom_container + ')');
		}
		if (!(options instanceof Array)) {
			throw new TypeError('populateCheckboxes() - options is not an Array. (' + typeof options + ')');
		}

		let name_value = name_attribute;
		if (typeof name_value !== 'string') {
			name_value = 'checkbox';
		}

		// Do work
		let count = 0;
		let html = '';
		for (let i = 0, len = options.length; i < len; i++) {
			if (typeof options[i] !== 'object') {
				continue;
			}

			let value = Object.keys(options[i])[0];
			let text = options[i][value];
			let id = name_value + '-' + checkbox_radio_counter++;

			html += '' +
				'<input type="checkbox" id="' + id + '" name="' + name_value + '" value="' + value + '" />' +
				'<label for="' + id + '">' + text + '</label>';
			count++;
		}
		dom_container.innerHTML = html;

		return count;
	} // populateCheckboxes()

	/**
	 * Creates radio children using an array of object literals.
	 * - Note: any existing HTML in the div will be wiped out
	 * - Note: the first radio will be checked
	 * @param {HTMLElement} dom_container - container for generated radios
	 * @param {Array} options - objects in the format of {"value": "Text to show"}
	 * @param {string} name_attribute - name attribute of the radios. Optional. (DEFAULTS to "radio")
	 * @return {number} count of radios added
	 */
	function populateRadios(dom_container, options, name_attribute) {
		// Check params
		if (!(dom_container instanceof HTMLElement)) {
			throw new TypeError('populateRadios() - param is not an HTMLElement. (' + typeof dom_container + ')');
		}
		if (!(options instanceof Array)) {
			throw new TypeError('populateRadios() - options is not an Array. (' + typeof options + ')');
		}

		let name_value = name_attribute;
		if (typeof name_value !== 'string') {
			name_value = 'radio';
		}

		// Do work
		let count = 0;
		let html = '';
		for (let i = 0, len = options.length; i < len; i++) {
			if (typeof options[i] !== 'object') {
				continue;
			}

			let value = Object.keys(options[i])[0];
			let text = options[i][value];
			let id = name_value + '-' + checkbox_radio_counter++;

			let checked_text = '';
			if (i === 0) {
				checked_text = 'checked="checked"';
			}

			html += '' +
				'<input type="radio" id="' + id + '" name="' + name_value + '" value="' + value + '"' + checked_text + ' />' +
				'<label for="' + id + '">' + text + '</label>';
			count++;
		}
		dom_container.innerHTML = html;

		return count;
	} // populateRadios()

	/**
	 * Removes options from a select / datalist
	 * @param {HTMLSelectElement | HTMLDataListElement} dom_elem
	 * @param {string} value of the option
	 */
	function removeOption(dom_elem, value) {
		if (!(dom_elem instanceof HTMLElement)) {
			throw new TypeError('removeOption() - param is not an HTMLElement. (' + typeof dom_elem + ')');
		}

		let dom_options = [].slice.call(dom_elem.options);
		for (let i = dom_options.length - 1; i >= 0; --i) {
			if (dom_options[i].value === value) {
				dom_options[i].remove();
		    }
		}
	} // removeOption()

	/**
	 * Removes a checkbox from a container of checkboxes
	 *  - Note: checkboxes NEED to have a <label> immediately after <input>.
	 * @param {HTMLElement} dom_container - container of checkboxes
	 * @param {string} value of the checkbox
	 */
	function removeCheckbox(dom_container, value) {
		// Check params
		if (!(dom_container instanceof HTMLElement)) {
			throw new TypeError('removeCheckbox() - param is not an HTMLElement. (' + typeof dom_container + ')');
		}

		let dom_elems = [].slice.call(dom_container.querySelectorAll('input[type="checkbox"]'));

		for (let i = dom_elems.length - 1; i >= 0; --i) {
			if (dom_elems[i].value === value) {
				let dom_parent = dom_elems[i].parentNode;
				let dom_label = dom_elems[i].nextElementSibling;
				dom_parent.removeChild(dom_elems[i]);
				dom_parent.removeChild(dom_label);
		    }
		}
	} // removeCheckbox()

	/**
	 * Removes a radio from a container of radios
	 *  - Note: checkboxes NEED to have a <label> immediately after <input>.
	 * @param{HTMLElement} dom_container - container of radios
	 * @param {string} value of the radio
	 */
	function removeRadio(dom_container, value) {
		// Check params
		if (!(dom_container instanceof HTMLElement)) {
			throw new TypeError('removeRadio() - param is not an HTMLElement. (' + typeof dom_container + ')');
		}

		let dom_elems = [].slice.call(dom_container.querySelectorAll('input[type="radio"]'));

		for (let i = dom_elems.length - 1; i >= 0; --i) {
			if (dom_elems[i].value === value) {
				let dom_parent = dom_elems[i].parentNode;
				let dom_label = dom_elems[i].nextElementSibling;
				dom_parent.removeChild(dom_elems[i]);
				dom_parent.removeChild(dom_label);
		    }
		}
	} // removeRadio()

	/**
	 * Ticks checkboxes with these values
	 * @param {Array} of HTMLInputElement checkboxes
	 *        {Array} of strings
	 *        {Object=} opts - optional.
	 *        - {boolean} csv - whether checkboxes are allowed to contain csv (multi-values)
	 *                          (note: csv must contain all values in params to be checked)
	 */
	function setCheckboxes(dom_checkboxes, values, opts) {
		// Check options
		let options = Object.assign({'csv': false}, opts);
		if (typeof options['csv'] !== 'boolean') {
			options['csv'] = false;
		}

		// Go through all checkboxes
		for (let i = 0, len = dom_checkboxes.length; i < len; i++) {
			if (options['csv']) {
				let tmp = dom_checkboxes[i].value.split(',');
				dom_checkboxes[i].checked = arrayContainsArray(values, tmp);
			}
			else {
				let tmp = dom_checkboxes[i].value;
				dom_checkboxes[i].checked = values.includes(tmp);
			}
		}
	} // setCheckboxes()

	/**
	 * Checks if a select has a particular option value
	 * @param {HTMLSelectElement | HTMLDataListElement} dom_elem
	 * @param {string} value of the option
	 * @return {bool}
	 */
	function hasOption(dom_elem, value) {
		if (!(dom_elem instanceof HTMLElement)) {
			throw new TypeError('hasOption() - param is not an HTMLElement. (' + typeof dom_elem + ')');
		}

		for (let i = 0, len = dom_elem.options.length; i < len; i++) {
			if (dom_elem.options[i].value === value) {
				return true;
			}
		}

		return false;
	} // hasOption()

	/**
	 * Uses File Reader API to read the contents of the file
	 * @param {HTMLInputElement} dom_input
	 * @param {Object=} opts - optional.
	 *                  - {string} encoding [utf-8, base64] (defaults to 'base64')
	 * @return {Promise} resolves with {string}, rejects on error
	 */
	function readFile(dom_input, opts) {
		// Check params
		if (!(dom_input instanceof HTMLInputElement)) {
			throw new TypeError('readFile() - Expecting param1 to be HTMLInputElement. Got: ' + dom_input);
		}
		if (dom_input.type !== 'file') {
			throw new TypeError('readFile() - Expecting param1 to be type="file". Got: ' + dom_input.type);
		}

		let options = Object.assign({'encoding': 'base64'}, opts);
		if (typeof options !== 'object' || options === null) {
			options = {
				'encoding': 'base64'
			};
		}
		if (typeof options['encoding'] !== 'string' || (options['encoding'] !== 'base64' && options['encoding'] !== 'utf-8')) {
			options['encoding'] = 'base64';
		}

		return new Promise(function(resolve, reject) {
			if (dom_input.files && dom_input.files[0]) {
				let reader = new FileReader();
				reader.onload = function() {
					resolve(reader.result);
				};
				if (options['encoding'] === 'base64') {
					reader.readAsDataURL(dom_input.files[0]);
				}
				else if (options['encoding'] === 'utf-8') {
					reader.readAsText(dom_input.files[0], 'UTF-8');
				}
			}
			else {
				reject(new Error('No file found from input'));
			}
		});
	} // readFile()

	/**
	 * Uses File Reader API to read the contents of the file
	 * @param {HTMLInputElement} dom_input
	 * @param {Object=} opts - optional.
	 *                  - {string} encoding [utf-8, base64] (defaults to 'base64')
	 * @return {Promise} resolves with {string}, rejects on error
	 */
	function readFileList(dom_input, opts) {
		// Check params
		if (!(dom_input instanceof HTMLInputElement)) {
			throw new TypeError('readFileList() - Expecting param1 to be HTMLInputElement. Got: ' + dom_input);
		}
		if (dom_input.type !== 'file') {
			throw new TypeError('readFileList() - Expecting param1 to be type="file". Got: ' + dom_input.type);
		}

		let options = Object.assign({'encoding': 'base64'}, opts);
		if (typeof options !== 'object' || options === null) {
			options = {
				'encoding': 'base64'
			};
		}
		if (typeof options['encoding'] !== 'string' || (options['encoding'] !== 'base64' && options['encoding'] !== 'utf-8')) {
			options['encoding'] = 'base64';
		}
		
		if (!dom_input.files || !dom_input.files[0]) {
			throw new Error('No file found from input');
		}

		// Build array of promises
		let promises = [];
		let files = Array.from(dom_input.files);
		files.forEach(function(file) {
			let promise = new Promise(function(resolve, reject) {
				let reader = new FileReader();
				reader.onload = function() {
					resolve(reader.result);
				};
				if (options['encoding'] === 'base64') {
					reader.readAsDataURL(file);
				}
				else if (options['encoding'] === 'utf-8') {
					reader.readAsText(file, 'UTF-8');
				}
			});
			promises.push(promise);
		});

		return Promise.all(promises);
	} // readFileList()

	/**
	 * - makes all <input> and <textarea> in the form READONLY
	 * - disables all options other than the current selected one in all <select>
	 * - disables all <button type="submit"> within form
	 * @param {HTMLFormElement} dom_form
	 * @return {boolean} success
	 */
	function disableForm(dom_form) {
		if (!(dom_form instanceof HTMLFormElement)) {
			throw new TypeError('disableForm() - param is not HTMLFormElement. (' + typeof dom_form + ')');
		}

		// Don't double disable
		if (isDisabledForm(dom_form)) {
			return false;
		}

		// Disable input, textarea, select
		for (let i = 0, len = dom_form.elements.length; i < len; i++) {
			let dom_elem = dom_form.elements[i];
			let node_name = dom_elem.nodeName.toUpperCase();

			if (node_name === 'INPUT' || node_name === 'TEXTAREA') {
				dom_elem.setAttribute(DISABLED_FORM_ORIG_READONLY_DATA_ATTRIBUTE, dom_elem.readOnly);
				dom_elem.readOnly = true;
			}
			else if (node_name === 'SELECT') {
				for (let j = 0, lenj = dom_elem.options.length; j < lenj; j++) {
					if (j === dom_elem.selectedIndex) {
						continue;
					}
					dom_elem.options[j].setAttribute(DISABLED_FORM_ORIG_DISABLED_DATA_ATTRIBUTE, dom_elem.options[j].disabled);
					dom_elem.options[j].disabled = true;
				}
			}
		} // for each element

		// Disable submit buttons
		let dom_submit_buttons = dom_form.querySelectorAll('button[type="submit"]');
		for (let i = 0, len = dom_submit_buttons.length; i < len; i++) {
			dom_submit_buttons[i].disabled = true;
		}

		dom_form.setAttribute(DISBALED_FORM_DATA_ATTRIBUTE, 'true');

		return true;
	} // disableForm()

	/**
	 * Undos all effects done by disableForm()
	 * @param {HTMLFormElement} dom_form
	 * @return {boolean} success
	 */
	function enableForm(dom_form) {
		if (!(dom_form instanceof HTMLFormElement)) {
			throw new TypeError('enableForm() - param is not HTMLFormElement. (' + typeof dom_form + ')');
		}

		// Don't handle forms that weren't disabled
		if (!isDisabledForm(dom_form)) {
			return false;
		}

		// Revert input, textarea, select
		for (let i = 0, len = dom_form.elements.length; i < len; i++) {
			let dom_elem = dom_form.elements[i];
			let node_name = dom_elem.nodeName.toUpperCase();

			if (node_name === 'INPUT' || node_name === 'TEXTAREA') {
				let orig_readonly = dom_elem.getAttribute(DISABLED_FORM_ORIG_READONLY_DATA_ATTRIBUTE);
				dom_elem.removeAttribute(DISABLED_FORM_ORIG_READONLY_DATA_ATTRIBUTE);
				dom_elem.readOnly = orig_readonly === 'true';
			}
			else if (node_name === 'SELECT') {
				for (let j = 0, lenj = dom_elem.options.length; j < lenj; j++) {
					let orig_disabled = dom_elem.options[j].getAttribute(DISABLED_FORM_ORIG_DISABLED_DATA_ATTRIBUTE);
					dom_elem.options[j].removeAttribute(DISABLED_FORM_ORIG_DISABLED_DATA_ATTRIBUTE);
					dom_elem.options[j].disabled = orig_disabled === 'true';
				}
			}
		} // for each element

		// Revert submit buttons
		let dom_submit_buttons = dom_form.querySelectorAll('button[type="submit"]');
		for (let i = 0, len = dom_submit_buttons.length; i < len; i++) {
			dom_submit_buttons[i].disabled = false;
		}

		dom_form.removeAttribute(DISBALED_FORM_DATA_ATTRIBUTE);

		return true;
	} // enableForm()

	/**
	 * Checks if a form has been disabled through disableForm()
	 * @param {HTMLFormElement} dom_form
	 * @return {boolean}
	 */
	function isDisabledForm(dom_form) {
		if (!(dom_form instanceof HTMLFormElement)) {
			throw new TypeError('isDisabledForm() - param is not HTMLFormElement. (' + typeof dom_form + ')');
		}

		return dom_form.getAttribute(DISBALED_FORM_DATA_ATTRIBUTE) === 'true';
	} // isDisabledForm()

	/**
	 * Finds a element's output and shows an error message for a specified duration.
	 * - Sequence to find output:
	 *    1) Checks if the element is an output itself
	 *    2) Searches by id output[for="id-of-elem"]
	 *    3) Searches inside descendants
	 *    4) Searches among siblings
	 *    5) Searches ancestor's siblings
	 * - Expects output to be hidden unless classes are added to it
	 * @param {string} msg
	 * @param {HTMLElement} dom_elem
	 * @param {Object} opts
	 *            {number} duration - how long the output will be shown for, in ms. Set to 0 to show forever.
	 *            {boolean} scroll - whether to make the message animate scroll into view
	 *            {string | Array} addClass - class name(s) to append to the output while it is being shown
	 *            {string | Array} removeClass - class name(s) to remove from the output while it is being shown
	 *            {boolean} escape - whether to escape the msg. Set to false if msg is HTML
	 * @return {number} timer id. zero if duration is 0.
	 */
	function outputFormMessage(msg, dom_elem, opts) {
		if (typeof msg !== 'string') {
			throw new TypeError('outputFormMessage() - msg is not string. (' + typeof msg + ')');
		}
		if (!(dom_elem instanceof HTMLElement)) {
			throw new TypeError('outputFormMessage() - param is not HTMLElement. (' + typeof dom_elem + ')');
		}

		let options = processFormOutputOptions(opts);

		// Find output to write message in
		let dom_output;
		let dom_elem_outputs, elem_id, output_count, dom_ancestor; // tmp variables used to find output to use
		if (dom_elem.tagName === 'OUTPUT') { // don't depend on "instancof HTMLOutputElement" as IE does not support it
			dom_output = dom_elem;
		}
		if (!dom_output) {
			elem_id = dom_elem.getAttribute('id');
			if (elem_id) {
				dom_output = findOne('output[for="' + elem_id + '"]');
			}
		}
		if (!dom_output) {
			// console.debug('outputFormMessage() - cannot find output from id.', dom_elem);
			dom_elem_outputs = findAll('output', dom_elem);
			output_count = dom_elem_outputs.length;
			if (output_count > 0) {
				dom_output = dom_elem_outputs[output_count - 1]; // use the last output
			}
		}
		if (!dom_output) {
			// console.debug('outputFormMessage() - cannot find output among descendants.', dom_elem);
			dom_ancestor = dom_elem.parentElement;
			while (dom_ancestor) {
				// console.debug('Looking through ', dom_ancestor);
				elem_id = dom_ancestor.getAttribute('id');
				if (elem_id) {
					dom_output = findOne('output[for="' + elem_id + '"]');
				}
				if (dom_output) {
					dom_ancestor = null; // break loop
				}
				else {
					dom_elem_outputs = findAll('output', dom_ancestor);
					output_count = dom_elem_outputs.length;
					if (output_count > 0) {
						dom_output = dom_elem_outputs[output_count - 1]; // use the last output found
						dom_ancestor = null; // break loop
						// console.debug('Found output ', dom_output);
					}
					else {
						dom_ancestor = dom_ancestor.parentElement;
					}
				}
			}
		}
		if (!dom_output) {
			// console.debug('outputFormMessage() - cannot find output anywhere for', dom_elem);
			throw new RangeError('outputFormMessage() - no output found for this element.');
		}

		// Set text and show it
		if (options['escape']) {
			dom_output.innerHTML = escapeHTML(msg);
		}
		else {
			dom_output.innerHTML = msg;
		}
		options['addClass'].forEach(function(c) {
			dom_output.classList.add(c);
		});
		options['removeClass'].forEach(function(c) {
			dom_output.classList.remove(c);
		});
		if (options['scroll']) {
			animateScrollIntoView(dom_output);
		}

		// Clear any old timers
		let timer_id = dom_output.getAttribute(OUTPUT_TIMER_DATA_ATTRIBUTE);
		if (timer_id) {
			timer_id = parseInt(timer_id);
			clearTimeout(timer_id);
		}

		// Set timer (if needed) to clear classes later on
		if (options['duration'] === 0) {
			dom_output.removeAttribute(OUTPUT_TIMER_DATA_ATTRIBUTE);
			return 0;
		}

		timer_id = setTimeout(function() {
			options['addClass'].forEach(function(c) {
				dom_output.classList.remove(c);
			});
			options['removeClass'].forEach(function(c) {
				dom_output.classList.add(c);
			});
			dom_output.removeAttribute(OUTPUT_TIMER_DATA_ATTRIBUTE);
		}, options['duration']);
		dom_output.setAttribute(OUTPUT_TIMER_DATA_ATTRIBUTE, timer_id);
		return timer_id;
	} // outputFormMessage()

	/**
	 * Converts Form element into a CSV.
	 * - Note:
	 *       if parseHeader is true, returns a line containing all names/IDs, and a line containing all values.
	 *       if praseHeader is false, returns a single-line CSV of values only.
	 *       either name or id attribute need to be present, else element is ignored.
	 *
	 * @param {HTMLFormElement} dom_form
	 * @param {Object=} opts (optional)
	 *                  - {string} delimiter (defaults to comma. Must be 1 char)
	 *                  - {string} lineEnding (defaults to \n. Max of 2 chars)
	 *                  - {string} quote {defaults to ". Must be 1 char)
	 *                  - {boolean} parseHeader (defaults to true)
	 *                  - {boolean} trim (defaults to true)
	 *                  - {boolean} unescapeHTML (defaults to true for table elements, false otherwise)
	 * @return {string}
	 */
	function form2csv(dom_form, opts) {
		if (!(dom_form instanceof HTMLFormElement)) {
			throw new TypeError('form2csv() - param is not a HTMLFormElement.');
		}

		// Process options
		let options = processCSVOptions(opts);
		if (typeof options['unescapeHTML'] !== 'boolean') {
			options['unescapeHTML'] = false;
		}

		let nameData = [];
		let valueData = [];
		for (let i = 0, row_count = dom_form.elements.length; i < row_count; i++) {
			let n, v;
			try {
				n = processCSVCellValue(dom_form.elements[i].name, options) || processCSVCellValue(dom_form.elements[i].id, options);
				v = processCSVCellValue(dom_form.elements[i].value, options);
				if (options['unescapeHTML']) {
					n = unescapeHTML(n);
					v = unescapeHTML(v);
				}
			}
			catch (e) {
				continue;
			}

			nameData.push(n);
			valueData.push(v);
		}

		if (options['parseHeader']) {
			return nameData.join(options['delimiter']) + options['lineEnding'] + valueData.join(options['delimiter']);
		}

		return valueData.join(options['delimiter']);
	} // form2csv()

/******************
*  Table related  *
*******************/
	/**
	 * Converts Table element into a CSV.
	 *       if parseHeader is false, rows in <thead> will be skipped.
	 *       <thead> is expected to occur at most once, and only at the beginning.
	 *
	 * @param {HTMLTableElement} dom_table
	 * @param {Object=} opts (optional)
	 *                  - {string} delimiter (defaults to comma. Must be 1 char)
	 *                  - {string} lineEnding (defaults to \n. Max of 2 chars)
	 *                  - {string} quote {defaults to ". Must be 1 char)
	 *                  - {boolean} parseHeader (defaults to true)
	 *                  - {boolean} trim (defaults to true)
	 *                  - {boolean} unescapeHTML (defaults to true for table elements, false otherwise)
	 * @return {string}
	 */
	function table2csv(dom_table, opts) {
		if (!(dom_table instanceof HTMLTableElement)) {
			throw new TypeError('table2csv() - param is not a HTMLTableElement.');
		}

		// Process options
		let options = processCSVOptions(opts);
		if (typeof options['unescapeHTML'] !== 'boolean') {
			options['unescapeHTML'] = true;
		}

		let csvData = [];

		let row_count = dom_table.rows.length;
		for (let i = options['parseHeader'] ? 0 : dom_table.tHead.rows.length; i < row_count; i++) {
			let row = dom_table.rows[i];

			let rowData = [];
			for (let j = 0, cell_count = row.cells.length; j < cell_count; j++) {
				let value = row.cells[j].innerHTML;
				if (options['unescapeHTML']) {
					value = unescapeHTML(value);
				}

				rowData.push(processCSVCellValue(value, options));
			}
			csvData.push(rowData.join(options['delimiter']));
		} // for each row

		return csvData.join(options['lineEnding']);
	} // table2csv()

	global['domo'] = {

		/* DOM document functions */
		'getMeta': getMeta,
		'setMeta': setMeta,
		'addCSSFile': addCSSFile,
		'addCSSLink': addCSSFile,
		'removeCSSFile': removeCSSFile,
		'removeCSSLink': removeCSSFile,
		'removeCSSFileByData': removeCSSFileByData,
		'removeCSSLinkByData': removeCSSFileByData,
		'addJSFile': addJSFile,
		'removeJSFile': removeJSFile,
		'removeJSFileByData': removeJSFileByData,
		'str2dom': str2dom,
		'getSelectedText': getSelectedText,

		/* DOM element functions */
		'selectText': selectText,
		'clearSelection': clearSelection,
		'wrapNode': wrapNode,
		'lockScroll': lockScroll,
		'unlockScroll': unlockScroll,
		'isLockedScroll': isLockedScroll,
		'getPositionOnScreen': getPositionOnScreen,
		'getPositionOnPage': getPositionOnPage,
		'isMiddleInViewport': isMiddleInViewport,
		'isPartiallyInViewport': isPartiallyInViewport,
		'animateScroll': animateScroll,
		'animateScrollIntoView': animateScrollIntoView,
		'findOne': findOne,
		'findAll': findAll,
		'on': on,
		'off': off,
		'getListenerCount': getListenerCount,

		/* Form functions */
		'populateFormValues': populateFormValues,
		'populateForm': populateFormValues,
		'populateSelect': populateSelect,
		'populateSelects': populateSelects,
		'populateDatalist': populateDatalist,
		'populateCheckboxes': populateCheckboxes,
		'populateRadios': populateRadios,
		'removeOption': removeOption,
		'removeCheckbox': removeCheckbox,
		'removeRadio': removeRadio,
		'setCheckboxes': setCheckboxes,
		'hasOption': hasOption,
		'readFile': readFile,
		'readFileList': readFileList,
		'enableForm': enableForm,
		'disableForm': disableForm,
		'isDisabledForm': isDisabledForm,
		'outputFormMessage': outputFormMessage,
		'form2csv': form2csv,

		/* Table functions */
		'table2csv': table2csv
	};
}(this, window, document));
